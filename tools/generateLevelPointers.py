#!/usr/bin/python

import xml.etree.ElementTree as etree    
import sys
import os
import re
import ntpath
import glob

def generate_level_pointers():

    levels = glob.glob('levels/*.lvl')
    levels.sort()

    mapsPointersL = ""
    mapsPointersH = ""
    maxPointersL = ""
    maxPointersH = ""
    text1PointersL = ""
    text1PointersH = ""
    text2PointersL = ""
    text2PointersH = ""
    inst0PointersL = ""
    inst0PointersH = ""
    inst1PointersL = ""
    inst1PointersH = ""
    externs = ""

    for level_file in levels:
        print "processing " + level_file
        file_name = ntpath.basename(level_file)
        level_id = os.path.splitext(file_name)[0]

        mapsPointersL += "  .byt <_" + level_id + "\n"
        mapsPointersH+= "  .byt >_" + level_id + "\n"

        maxPointersL += "  .byt <_" + level_id + "_max\n"
        maxPointersH+= "  .byt >_" + level_id + "_max\n"

        text1PointersL += "  .byt <_text_" + level_id + "\n"
        text1PointersH += "  .byt >_text_" + level_id + "\n"

        text2PointersL += "  .byt <_text_" + level_id + "title\n"
        text2PointersH += "  .byt >_text_" + level_id + "title\n"

        inst0PointersL += "  .byt <_text_" + level_id + "inst0\n"
        inst0PointersH += "  .byt >_text_" + level_id + "inst0\n"

        inst1PointersL += "  .byt <_text_" + level_id + "inst1\n"
        inst1PointersH += "  .byt >_text_" + level_id + "inst1\n"

        externs += ".import  _" + level_id + ";\n"
        externs += ".import _" + level_id + "_max;\n"
        externs += ".import _text_" + level_id + ";\n"
        externs += ".import _text_" + level_id + "title;\n"

        externs += ".import _text_" + level_id + "inst0;\n"
        externs += ".import _text_" + level_id + "inst1;\n"

    

    output_file = open("bin/gen/levelPointers.s", "w")

    output_file.write(externs)
    output_file.write(".SEGMENT \"RODATA\"\n")


    output_file.write(".export _level_mapsLo\n")
    output_file.write("_level_mapsLo:\n")
    output_file.write(mapsPointersL)

    output_file.write(".export _level_mapsHi\n")
    output_file.write("_level_mapsHi:\n")
    output_file.write(mapsPointersH)

    output_file.write(".export _level_maxesLo\n")
    output_file.write("_level_maxesLo:\n")
    output_file.write(maxPointersL)

    output_file.write(".export _level_maxesHi\n")
    output_file.write("_level_maxesHi:\n")
    output_file.write(maxPointersH)

    output_file.write(".export _level_text1PointersLo\n")
    output_file.write("_level_text1PointersLo:\n")
    output_file.write(text1PointersL)

    output_file.write(".export _level_text1PointersHi\n")
    output_file.write("_level_text1PointersHi:\n")
    output_file.write(text1PointersH)

    output_file.write(".export _level_text2PointersLo\n")
    output_file.write("_level_text2PointersLo:\n")
    output_file.write(text2PointersL)

    output_file.write(".export _level_text2PointersHi\n")
    output_file.write("_level_text2PointersHi:\n")
    output_file.write(text2PointersH)


    output_file.write(".export _level_inst0PointersLo\n")
    output_file.write("_level_inst0PointersLo:\n")
    output_file.write(inst0PointersL)

    output_file.write(".export _level_inst0PointersHi\n")
    output_file.write("_level_inst0PointersHi:\n")
    output_file.write(inst0PointersH)


    output_file.write(".export _level_inst1PointersLo\n")
    output_file.write("_level_inst1PointersLo:\n")
    output_file.write(inst1PointersL)

    output_file.write(".export _level_inst1PointersHi\n")
    output_file.write("_level_inst1PointersHi:\n")
    output_file.write(inst1PointersH)

    output_file.close()


if __name__ == "__main__":
    generate_level_pointers()
    
