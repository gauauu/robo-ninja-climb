#!/usr/bin/python


def main():
    import sys
    import re
    argv = sys.argv
    if len(argv) < 3:
        print "Must supply source and destination files"
        sys.exit(1)
    outfile = open(argv[2], "w")
    infile = open(argv[1], "r")
    outfile.write("\n.segment \"CODE\"\n\n")
    lines = infile.readlines()
    for line in lines:
        line = line.strip()
        if line == "":
            continue

        parts = line.split(":")
        subject = parts[0]
        label = parts[0].replace(" ", "_");
        if len(parts) > 1:
            subject = parts[1].lower()
        chars = len(subject)
        outfile.write(".export _text_" + label + "\n")
        outfile.write("_text_" + label + ":\n")
        outfile.write(".byte " + str(chars) + "\n")
        for c in subject[::-1]:
            charNum = ord(c) - 32 + 99 + 6
            numberMatcher = re.compile("[0-9]")
            if (c == " "):
                outfile.write(".byte $ff ;space\n")
            elif (c == "|"):
                outfile.write(".byte 255; pipe - break\n")
            elif (c == "!"):
                outfile.write(".byte $c4 ;!\n")
            elif (c == ","):
                outfile.write(".byte $ca ;,\n")
            elif (c == "/"):
                outfile.write(".byte $ce ;/\n")
            elif (c == "."):
                outfile.write(".byte $cf ;.\n")
            elif (c == ";"):
                outfile.write(".byte $c6 ; ; but meaning :\n")
            elif (c == "-"):
                outfile.write(".byte $c7 ; -\n")
            elif (c == "?"):
                outfile.write(".byte $cd ;?\n")
            elif (c == "'"):
                outfile.write(".byte $c9 ;'\n")
            elif (numberMatcher.match(c)):
                charNum = 160 + int(c)
                outfile.write(".byte " + str(charNum) + ";" + c + "\n")
            else: 
                outfile.write(".byte " + str(charNum) + ";" + c + "\n")
        outfile.write(";.byte \"" + subject + "\"\n\n")


if __name__=='__main__':
    main()
