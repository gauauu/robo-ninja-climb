#!/usr/bin/make -f

###################################
# Makefile for robo-ninja-climb
###################################

####################################
# Project-specific configurations
####################################

title = robo-ninja-climb


####################################
# Build tool and path configuration
####################################
ifndef CC65_PATH
$(error Environment variable CC65_PATH is not set - must be set to the root path of cc65 installation)
endif

CC65_SHARE = $(CC65_PATH)/share/cc65

OBJDIR = bin/obj
GENDIR = bin/gen
SRCDIR = src
GFXDIR = gfx
PALDIR = gfx/pal
DATADIR = data
CBUILDFLAGS = -g -Oir -I $(CC65_SHARE)/include -I $(GENDIR) -I $(SRCDIR)
SBUILDFLAGS = -g -I $(CC65_SHARE)/asminc -I $(GENDIR)
CA65 = ca65 $(SBUILDFLAGS)
CC65 = cc65 $(CBUILDFLAGS)
LD65 = ld65
LINKFLAGS = -L $(CC65_PATH)/share/cc65/lib -m map.txt -Ln labels.txt --dbgfile $(title).dbg

DEBUGMAPS = $(title).nes.ram.nl $(title).nes.0.nl


#########################################
# Wildcard classes
#########################################

C_ASMS   = $(patsubst $(SRCDIR)/%.c, $(GENDIR)/%.s, $(wildcard $(SRCDIR)/*.c))
C_OBJS   = $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(wildcard $(SRCDIR)/*.c))
ASM_OBJS = $(patsubst $(SRCDIR)/%.s, $(OBJDIR)/%.o, $(wildcard $(SRCDIR)/*.s))
DATA		 = $(wildcard $(DATADIR)/*.*)
LEVELS	 = $(patsubst levels/%.lvl, $(OBJDIR)/%.o, $(wildcard levels/*.lvl)) $(OBJDIR)/levelPointers.o
HEADERS  = $(wildcard $(SRCDIR)/*.h)
TEXTDATA = $(OBJDIR)/textData.o

MKDIRS   = @mkdir -p $(OBJDIR) && mkdir -p $(GENDIR)

ALL_OBJS = $(C_OBJS) $(ASM_OBJS) $(LEVELS) $(TEXTDATA)

#####################################
# Emulators, running, etc
#####################################

ifndef EMU
EMU = fceux
endif

ifndef DEBUGGER
DEBUGGER := wine /home/tolbert/tools/fceux/fceux.exe z:\\home\\tolbert\\dev\\misc\\rnjump\\
endif

ifndef NOCASH
NOCASH := wine /opt/nocash/NOCASH.EXE z:\\home\\tolbert\\dev\\blastervania\\
endif

#####################################
# Main Targets
#####################################

.PHONY:	run clean maps debug nocash

#link the objs 
$(title).nes : $(ALL_OBJS) $(GENDIR)/sprite.chr $(GENDIR)/sprite2.chr $(DATA)
	rm -rf src/.*.*.sw*
	$(LD65) $(LINKFLAGS) -C nrom128.cfg $(ALL_OBJS) nes.lib -o $@

run: $(title).nes
	$(EMU) $<

debug: $(title).nes $(DEBUGMAPS)
	$(DEBUGGER) $<

nocash: $(title).nes 
	$(NOCASH) $<

clean: 
	-rm -rf $(OBJDIR) $(GENDIR) *.nes *.nl map.txt labels.txt

maps: $(DEBUGMAPS)

idea: CMakeLists.txt
	echo "not implemented"

CMakeLists.txt: $(wildcard $(SRCDIR)/*.*)
	echo "not implemented"

$(DEBUGMAPS): labels.txt
	./tools/fceux_symbols.py labels.txt $(title).nes


########################################
# Compile rules
########################################

#compile C into transitional assembly
$(GENDIR)/%.s : $(SRCDIR)/%.c $(HEADERS) 
	$(MKDIRS)
	$(CC65) $(BUILDFLAGS) $< -o $@

#compile generated C into transitional assembly
$(GENDIR)/%.s : $(GENDIR)/%.c $(HEADERS)
	$(MKDIRS)
	$(CC65) $(BUILDFLAGS) $< -o $@

#compile C-generated assembly into obj
$(OBJDIR)/%.o : $(GENDIR)/%.s $(HEADERS)
	$(MKDIRS)
	$(CA65) $(BUILDFLAGS) $< -o $@

#compile assembly into obj
$(OBJDIR)/%.o : $(SRCDIR)/%.s $(HEADERS)
	$(MKDIRS)
	$(CA65) $(BUILDFLAGS) $< -o $@

$(OBJDIR)/data.o : $(SRCDIR)/data.s $(HEADERS) $(DATA)
	$(MKDIRS)
	$(CA65) $(BUILDFLAGS) $< -o $@

#chr.o requires chr to have been generated
$(OBJDIR)/chr.o : $(GENDIR)/sprite.chr $(GENDIR)/sprite2.chr

#build gfx with tepples' pilbmp2nes
$(GENDIR)/%.chr: $(GFXDIR)/%.png
	tools/pilbmp2nes.py $< $@

#convert level text format to binary source
$(GENDIR)/%.s : levels/%.lvl
	$(MKDIRS)
	tools/level2Data.py $<

#transform text data 
$(GENDIR)/textData.s: $(SRCDIR)/textData.txd tools/processText.py
	$(MKDIRS)
	tools/processText.py $< $@

#build array of pointers to level data
$(GENDIR)/levelPointers.s: $(wildcard levels/*.lvl) tools/generateLevelPointers.py
	$(MKDIRS)
	tools/generateLevelPointers.py

