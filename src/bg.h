#include "common.h"

extern u16 camera_y;
extern u16 camera_screen;
extern u16 camera_vy;

extern u8 bg_offscreenRow;
extern u16 bg_offscreenRowAddress;
extern u16 bg_offscreenAttrAddress;

extern u8 bg_tileModifier;

//extern u8 bg_renderQueueLo[];
//extern u8 bg_renderQueueHi[];
//extern u8 bg_renderQueueVal[];
//extern u8 bg_renderQueueVal2[];
//extern u8 bg_renderQueueIndex;


//render queue is now hi,lo,len,values
extern u8 bg_renderQueue[];
extern u8 bg_renderIndex;

extern u8 bg_attrRowToRender;
extern u8 bg_attrMirror[];


void bg_update(void);
void bg_render(void);
void bg_init(void);
void bg_prepBackground(void);
void bg_processRenderQueue(void);

//copies 8 attr bytes from 
//the source map (temple_nt) to
//bg_attrMirror, starting at offset temp
void bg_loadAttrMirrorRow(void);
