#include "common.h"

#define DRAW_TEXT(label,x,y) \
  ci = x; \
  cj = y; \
  tempPtr = &(label); \
  drawTextAtCiCj();


#define DRAW_TEXT_CENTERED(label,y) \
    cj = y;                 \
    tempPtr = &(label);     \
    drawTextCenteredCj();   


//addr of text in _tempPtr
//A is lo ppu addr
//X is hi ppu addr
void drawText(void);


//addr of text line in _tempPtr
//A is lo ppu addr
//X is hi ppu addr
void drawTextLines(void);

//;addr of text line in _tempPtr
//;A is lo ppu addr
//;X is hi ppu addr
void drawTextCentered(void);

/*
 * cj is y
 * tempPtr is text:
 *  tempPtr = &(text_foo);
 */
void drawTextCenteredCj(void);
void drawTextAtCiCj(void);
