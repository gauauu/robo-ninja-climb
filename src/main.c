#include "types.h"
#include "items.h"
#include "controller.h"
#include "common.h"
#include "ppu.h"
#include "sprite.h"
#include "mc.h"
#include "bg.h"
#include "misc.h"
#include "obstacle.h"
#include "title.h"
#include "dead.h"
#include "level.h"
#include "sfx.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"


extern void startUp(void);
extern void FamiToneUpdate(void);

u8 main_paused;


void main(void) {
    //turn off ppu
    startUp();

    //victory_run();
    sfx_init();

    mc_maxHealth = 2;
    title_run();
    ppu_screenOff();
    waitVBlank();
    level_init();
    ppu_loadPalette(0);
    sprite_init();
    items_init();

    bg_prepBackground();
    bg_init();
    obstacle_init();

    mc_init();

    waitVBlank();
    level_showLevelScreen();

    waitVBlank();
    ppu_screenOnMain();
    sprite_sprite0Skip = true;

    
    while (1) {
      waitVBlank();
      PPU.mask = 0x18;
      //first, vblank things:
      sprite_renderSprites();
      bg_processRenderQueue();
      ppu_showLeftNametableAtYScroll();

 

      //then other prep work
      FamiToneUpdate();
      randomU8();
      sprite_sprite0Render();



      if (level_complete) {
        level_showLevelScreen();
        continue;
      }


      
      //then update game logic
      ctrl_update();

      if (ctrl_new & PAD_START) {
        main_paused = !main_paused;
        sfx_playSound(SFX_POWERUP);
      }

      if (main_paused) {
        PPU.mask = B10011010;
      } else {
        sprite_sprite0Update();
        if (mc_status != MC_STATUS_DEAD) {
          bg_update();
          mc_update();
          items_update();
              //PPU.mask = 0x19;
          obstacle_updateAll();
        } else {
          dead_update();
        }
      }

      //render things
      if (mc_status != MC_STATUS_DEAD) {
        mc_render();
      } else {
        dead_render();
      }
      items_render();
      obstacle_renderAll();
        //PPU.mask = 0x18;

      sprite_clearRemainingSprites();
     

      if (sprite_sprite0Skip) {
        --sprite_sprite0Skip;
      } else {
        //now spin on sprite 0
        sprite_sprite0Wait();
      }
    }

}


