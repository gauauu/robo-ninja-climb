#include "mc.h"
#include "controller.h"
#include "sprite.h"
#include "level.h"
#include "items.h"
#include "bg.h"
#include "dead.h"
#include "sfx.h"

#define ENDING_TIME (level_current == 4 && \
                     level_index < 20)




#define MC_SLIDE_SPEED  300
#define MC_LEFT_BOUNDARY 37
#define MC_RIGHT_BOUNDARY 177

#define MC_START_Y 10

#define MC_JUMP_VELOCITY 1050

#define MC_JUMP_IMPULSE 760
#define MC_DOUBLE_JUMP_IMPULSE 1000

#define MC_X_VELOCITY 1800
#define MC_GRAVITY 150
#define MC_MAX_FALL 1000

#define MC_SWING_SPEED 300

#define MC_ASCEND_LIMIT 20

#define MC_TILE_CLING 0
#define MC_TILE_ASC   0x60
#define MC_TILE_DESC  0x6A

#define MC_ANIM_SPEED 10
#define MC_ANIM_MAX 3

#define MC_FADED_TIME 120

u16 mc_x;
u16 mc_y;

s16 mc_vx;
s16 mc_vy;

u8 mc_status;

u8 mc_ascendTimer;
u8 mc_aReleased;

u8 mc_topClipped;

u16 mc_screen;

u8 mc_renderY;

u8 mc_blockYMin;
u8 mc_blockYMax;

u8 mc_collideRight;
u8 mc_collideLeft;

u8 mc_animFrame;
u8 mc_animTimer;

u8 mc_timesJumped;
u16 mc_deaths;

u8 mc_boundaryInverted;

u8 mc_health;
u8 mc_maxHealth;

u8 mc_fadedTimer;

void mc_jump(void) {
  mc_topClipped = false;
  mc_ascendTimer = MC_ASCEND_LIMIT;
  if (mc_status == MC_STATUS_CLING_L) {
    mc_vx = MC_X_VELOCITY;
  } else if (mc_status == MC_STATUS_CLING_R) {
    mc_vx = -MC_X_VELOCITY;
  }
  //when double-jumping, reduce velocity a bit
  if (mc_timesJumped == 1) {
    mc_vx = mc_vx / 2;
  }
  mc_status = MC_STATUS_ASCENDING;
  mc_vy = -MC_JUMP_VELOCITY;
  mc_aReleased = false;
  ++mc_timesJumped;
}

void mc_checkWall(void) {

  if (MSB(mc_x) < MC_LEFT_BOUNDARY) {
    mc_status = MC_STATUS_CLING_L;
    mc_x = (MC_LEFT_BOUNDARY << 8);
  }
  if (MSB(mc_x) > MC_RIGHT_BOUNDARY) {
    mc_status = MC_STATUS_CLING_R;
    mc_x = (MC_RIGHT_BOUNDARY << 8);
  }

}

u8 mc_posDiff;

void mc_clampYRange(void) {
    if (MSB(mc_y) > 240) {
        if (mc_vy > 0) {
          mc_y += (16 << 8);
          if (mc_screen > 0) {
            --mc_screen;
          }
        } else {
          mc_y -= (16 << 8);
          mc_screen++;
        }
    }

}

void mc_incrementDeaths() {
  ++mc_deaths;
  //fake a BCD
  if ((mc_deaths & 0xF) == 0xA) {
    mc_deaths += 0x6;
  }
  if ((mc_deaths & 0xF0) == 0XA0) {
    mc_deaths += 0x60;
  }
  if ((mc_deaths & 0xF00) == 0XA00) {
    mc_deaths += 0x600;
  }
}


extern void start(void);
void mc_dead(void) {

  if (mc_fadedTimer > 0) {
    return;
  }

  sfx_playSound(SFX_HIT);

  if (mc_health > 0) {
    --mc_health;
    mc_fadedTimer = MC_FADED_TIME;
    return;
  }

  mc_incrementDeaths();
  mc_status = MC_STATUS_DEAD;
  dead_begin();
}




void mc_handleJumpingMovement(void) {
  if (ctrl_current & PAD_LEFT) {
    mc_vx -= MC_SWING_SPEED;
    if (mc_vx < -MC_X_VELOCITY) {
      mc_vx = -MC_X_VELOCITY;
    }
  }
  if (ctrl_current & PAD_RIGHT) {
    mc_vx += MC_SWING_SPEED;
    if (mc_vx > MC_X_VELOCITY) {
      mc_vx = MC_X_VELOCITY;
    }
  }
}


#define MC_ROCKET_HORIZ 500
#define MC_ROCKET_H_MAX 1200

void mc_rocketUpdate(void) {
  mc_vy = -camera_vy;
  if (ENDING_TIME) {
    mc_vy -= 400;
  }
  if (mc_renderY < 100) {
    mc_vy += 150;
  }
  
  if (ctrl_current & PAD_LEFT) {
    mc_vx -= MC_ROCKET_HORIZ;
  }
  if (ctrl_current & PAD_RIGHT) {
    mc_vx += MC_ROCKET_HORIZ;
  }
  if (mc_vx < -MC_ROCKET_H_MAX) {
    mc_vx = -MC_ROCKET_H_MAX;
  }
  if (mc_vx > MC_ROCKET_H_MAX) {
    mc_vx = MC_ROCKET_H_MAX;
  }

  mc_x += mc_vx;

  mc_vx = mc_vx / 2;

  mc_checkWall();
  mc_y += mc_vy;

  camera_vy += 1;

  mc_clampYRange();
  

}


void mc_update(void) {

  if (mc_fadedTimer > 0) {
    --mc_fadedTimer;
  }

  if (!(ctrl_current & PAD_A)) {
    mc_aReleased = true;
  }

  if (items_owned[ITEM_ROCKET]) {
    mc_rocketUpdate();
    goto calcRendering;
  
  }
  
  if (mc_status < MC_STATUS_ASCENDING) {
    mc_timesJumped = 0;
    if (ctrl_current & PAD_DOWN) {
      mc_vy = MC_SLIDE_SPEED;
      mc_y += mc_vy;
      mc_clampYRange();
    }

    //do animations
    --mc_animTimer;
    if (mc_animTimer == 0) {
      ++mc_animFrame;
      mc_animTimer = MC_ANIM_SPEED;
      if (mc_animFrame > MC_ANIM_MAX) {
        mc_animFrame = 0;
      }
    }

    if ((ctrl_current & PAD_UP) && (items_owned[ITEM_PAUSE_JUMP])) {
      mc_vy = -MC_SLIDE_SPEED;
      mc_y += mc_vy;
      mc_clampYRange();
    }
#ifdef CHEAT_CTRL
    if (ctrl_current & PAD_LEFT) {
      mc_x -= MC_SLIDE_SPEED;
    }
    if (ctrl_current & PAD_RIGHT) {
      mc_x += MC_SLIDE_SPEED;
    }
#endif

    if (mc_aReleased && ctrl_current & PAD_A) {
      mc_jump();
    }
  } else if (mc_status == MC_STATUS_ASCENDING){
    --mc_ascendTimer;
    ci = ctrl_current & PAD_A;

    if (!ci) {
       mc_vy = 0;
      mc_status = MC_STATUS_DESCENDING;
    }

    if (mc_ascendTimer == 0) {
      mc_status = MC_STATUS_DESCENDING;
    }
    mc_vy += MC_GRAVITY;

    if (mc_timesJumped == 2) {
      mc_vy = -MC_DOUBLE_JUMP_IMPULSE;
    } else {
      mc_vy = -MC_JUMP_IMPULSE;
    }

    if (mc_topClipped) {
      mc_vy = 0;
    }

    mc_handleJumpingMovement();

    mc_y += mc_vy;
    mc_clampYRange();

    mc_x += mc_vx;


    mc_checkWall();


  } else if (mc_status == MC_STATUS_DESCENDING) {

    if (mc_aReleased && ctrl_current & PAD_A) {
      if (mc_timesJumped < 2 && items_owned[ITEM_DOUBLE_JUMP]) {
        mc_jump();
      }
    }


#ifdef PAUSE_JUMP
    if (ctrl_current & PAD_B && items_owned[ITEM_PAUSE_JUMP]) {
      mc_vx = 0;
    }
#endif

    mc_vy += MC_GRAVITY;

    if (mc_vy > MC_MAX_FALL) {
        mc_vy = MC_MAX_FALL;
    }

    mc_handleJumpingMovement();

    mc_x += mc_vx;
    mc_y += mc_vy;
    mc_clampYRange();

    mc_checkWall();

  }

calcRendering:


  //calc render position
  cj = MSB(mc_y); //y

  temp = (240 - MSB(camera_y));
  if (camera_screen != mc_screen) {
    temp -= 16;
  }
  cj += temp;
  mc_renderY = cj;



  //check screen boundaries
  if ((mc_renderY > (220 - MC_COL_HEIGHT)) && (mc_renderY < (235 - MC_COL_HEIGHT))) {
    //make sure he's REALLY dead
    mc_health = 0;
    mc_fadedTimer = 0;
    mc_dead();
    return;
  } else if (mc_vy < 0 && ((mc_renderY < 10) || (mc_renderY > 235))) {
    mc_topClipped = true;
  }

  //cache position helpers for faster collisions
  temp = MSB(mc_y);
  temp -= 12;
  mc_blockYMin = temp / 8;

  //use 2 bytes for temporary value because it can
  //get too big and will wrap
  tempU16 = MSB(mc_y);
  tempU16 += MC_COL_HEIGHT;
  tempU16 = tempU16 / 8;
  mc_blockYMax = LSB(tempU16);
  if (mc_blockYMax > 30) {
    mc_blockYMax -= 30;
  }

  mc_boundaryInverted = false;
  if (mc_blockYMax < mc_blockYMin){
    mc_boundaryInverted = true;
  }



  mc_collideRight = MSB(mc_x) + MC_COL_WIDTH;
  mc_collideLeft = MSB(mc_x);


}

void mc_render(void) {
  ci = MSB(mc_x); //x
  cj = mc_renderY;

  //deal with being faded
  if (mc_fadedTimer && (timer & B00000001)) {
    return;
  }

  //set attr based on health
  temp2 = mc_health; //attr
  if (mc_maxHealth > 0) {
    temp2 = 2 - mc_health;
  }

  if (items_owned[ITEM_ROCKET]) {
    temp  = MC_TILE_ASC;
    tempA = 5; //width
    tempB = 7; //height
    if (mc_vx < 0) {
      sprite_drawBlockFlipped();
      //draw flame
      temp = 0xD1;

      temp2 = 3;
      tempA = 1;
      tempB = 2;
      ci = MSB(mc_x) + 28;
      cj = mc_renderY + 32;
      sprite_drawBlockFlipped();

    } else {
      sprite_drawBlock();

      //draw flame
      temp = 0xD1;
      temp2 = 3;
      tempA = 1;
      tempB = 2;
      ci = MSB(mc_x) + 5;
      cj = mc_renderY + 32;
      sprite_drawBlock();
    }
    return;

  }


  if (mc_status < MC_STATUS_ASCENDING) {
      
    temp = MC_TILE_CLING;  //start tile
    temp = temp + (mc_animFrame * 4);
    tempA = 4; //width
    tempB = 6; //height
    if (mc_status == MC_STATUS_CLING_L) {
      sprite_drawBlockFlipped();
    } else {
      sprite_drawBlock();
    }
  } else if (mc_status <= MC_STATUS_DESCENDING) {
    if (mc_vy < 0) {
      temp  = MC_TILE_ASC;
    } else {
      temp  = MC_TILE_DESC;
    }
    tempA = 5; //width
    tempB = 7; //height
    if (mc_vx < 0) {
      sprite_drawBlockFlipped();
    } else {
      sprite_drawBlock();
    }
  }
}


void mc_init(void) {
  mc_fadedTimer = 0;
  mc_health = mc_maxHealth;
  mc_x = (MC_LEFT_BOUNDARY << 8);
  mc_y = (MC_START_Y << 8);
  mc_status = MC_STATUS_CLING_L;
  mc_screen = 0;
  mc_animTimer = MC_ANIM_SPEED;
  mc_aReleased = false;

}
