.include "common.inc"

.export _sprite_oamIndex


.segment "BSS"

_sprite_oamIndex:   .res  1

.segment "CODE"

.export _sprite_draw
.proc _sprite_draw
  ;don't let it use sprite 0
  ldx _sprite_oamIndex
  bne :+
    inx
    inx
    inx
    inx

:
  ;y
    lda _cj
    sta OAM,x
  
  ;tile
    lda _temp
    sta OAM+1,x

  ;attr
    lda _temp2
    sta OAM+2,x

  ;x
    lda _ci
    sta OAM+3,x

  
  inx
  inx
  inx
  inx

  stx _sprite_oamIndex

  rts

.endproc



; Moves all sprites after sprite_oam_index to offscreen.
; resets sprite_oam_index to 0
.export _sprite_clearRemainingSprites
.proc _sprite_clearRemainingSprites

    ; First round the address down to a multiple of 4 so that it won't
    ; freeze should the address get corrupted.
    lda _sprite_oamIndex
    and #%11111100
    tax

    ; then loop through and set all the sprites to Y of $FF
    lda #$FF
:
    sta OAM,x
    inx
    inx
    inx
    inx
    bne :-
    stx _sprite_oamIndex

  rts
.endproc



;starts the sprite dma. Should be called at the beginning of vblank rendering
.export _sprite_renderSprites
.proc _sprite_renderSprites
    lda #0
    sta OAMADDR
    ldy #>OAM
    sty OAM_DMA

    rts
.endproc





.import _sprite0x
SEAM_SCROLL_Y = 224
.export _sprite_sprite0Wait
.proc _sprite_sprite0Wait

  ldx #$04
  ldy #SEAM_SCROLL_Y

:
  lda PPUSTATUS
  and #%01000000
  bne :-
  
  ;wait for the sprite 0 flag to set
:
  bit PPUSTATUS
  bvc :-

  ;now burn 71 cycles
  lda ($44,x)  ;6 each
  lda ($44,x)
  lda ($44,x)
  lda ($44,x)
  lda ($44,x)
  lda ($44,x)
  lda ($44,x)  
  lda ($44,x)
  lda ($44,x)
  lda ($44,x)
  lda ($44,x)  ;66

  ; change the scrolling
  ; step 1, write nametable << 2 to PPUADDR
  stx PPUADDR  ;68
  ; step 2  - y to PPUSCROLL
  sty PPUSCROLL ;71
  ; step 3  - x to PPUSCROLL
  lda #0
  sta PPUSCROLL
  ; step 4 -- evil voodoo, see http://wiki.nesdev.com/w/index.php/PPU_scrolling#Split_X.2FY_scroll
  lda #(((SEAM_SCROLL_Y & $F8) << 2) & $FF)
  sta PPUADDR


  rts

.endproc

.import _sprite_sprite0Y
.export _sprite_sprite0Render
.proc _sprite_sprite0Render

  lda #$e0
  sta _temp
  lda #$21
  sta _ci
  ;lda #200
  lda _sprite_sprite0Y+1
  sta _cj
  lda #0
  sta _temp2


  ;now render 0
    lda _cj
    sta OAM
  
  ;tile
    lda _temp
    sta OAM+1

  ;attr
    lda _temp2
    sta OAM+2

  ;x
    lda _ci
    sta OAM+3

  lda #4
  sta _sprite_oamIndex
 
  rts

.endproc


.segment "CODE"
.export _sprite_debugU8
.proc _sprite_debugU8
    ;draw the least-significant digit
    tay
    and #$0F
    clc
    adc #$F0
    sta _temp

    lda #$18
    sta _ci
    sta _cj
    lda #$0
    sta _temp2

    jsr _sprite_draw


    ;then the most-significant
    tya
    sec
    ror
    sec
    ror
    sec
    ror
    sec
    ror
    sta _temp
    lda #$10
    sta _ci
    jmp _sprite_draw

.endproc


.export _sprite_renderU8
.proc _sprite_renderU8
    ;draw the least-significant digit
    tay
    and #$0F
    clc
    adc #$F0
    sta _temp

    lda #$0
    sta _temp2

    jsr _sprite_draw


    ;then the most-significant
    tya
    sec
    ror
    sec
    ror
    sec
    ror
    sec
    ror
    sta _temp
    lda _ci
    sec
    sbc #8
    sta _ci
    jmp _sprite_draw

.endproc
