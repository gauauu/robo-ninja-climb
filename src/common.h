#ifndef _COMMON_H_
#define _COMMON_H_

//#define CHEAT_CTRL
//#define CHEAT_NOSCROLL
//#define CHEAT_SEL_PAUSE
//#define CHEAT_B_SKIP


#include "types.h"
#include "binary.h"

extern u8 ci;
#pragma zpsym ("ci");

extern u8 cj;
#pragma zpsym ("cj");

extern u8 temp;
#pragma zpsym ("temp");

extern u8 temp2;
#pragma zpsym ("temp2");

extern u8 temp3;
#pragma zpsym ("temp3");

extern s8 tempS8;
#pragma zpsym ("tempS8");

extern u8 * tempPtr;
#pragma zpsym ("tempPtr");

extern u8 * tempPtr2;
#pragma zpsym ("tempPtr2");

extern u8 VBLANK_FLAG;
#pragma zpsym ("VBLANK_FLAG");

extern u8 timer;
#pragma zpsym ("timer");

extern u8 * tempPtrA;
extern u8 * tempPtrB;
extern u8 * tempPtrC;
extern u8 * tempPtrD;
#pragma zpsym ("tempPtrA");
#pragma zpsym ("tempPtrB");
#pragma zpsym ("tempPtrC");
#pragma zpsym ("tempPtrD");

extern u16 tempU16;
#pragma zpsym ("tempU16");

extern s16 tempS16;
#pragma zpsym ("tempS16");


#define TICKCOUNT ((u16)(*(u16*)0x6b))

#define MSB(x)			((u8)(((u16)x)>>8))
#define LSB(x)			((u8)(((u16)x)&0xff))


extern u16 tempU16A;
extern u16 tempU16B;
extern u16 tempU16C;
extern u16 tempU16D;

extern u8 tempA;
extern u8 tempB;
extern u8 tempC;
extern u8 tempD;
extern u8 tempE;
extern u8 tempF;
extern u8 tempG;
extern u8 tempH;
extern u8 tempj;
extern u8 tempJ;
extern u8 tempK;

extern const u8 DIV_32_LOOKUP[];


#define LOAD_PTR_FROM_ARRAYS(dest,sourceLo,sourceHi,index)  \
  __asm__("ldx %v", index); \
  __asm__("lda %v,x", sourceLo); \
  __asm__("sta %v", dest); \
  __asm__("lda %v,x", sourceHi); \
  __asm__("sta %v+1", dest); \


#endif
