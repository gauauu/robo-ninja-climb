extern u8 sprite_oamIndex;

#define SPRITE_TILE_SELECTOR 0x50

void sprite_init(void);


/**
 * temp is sprite index
 * ci is X
 * cj is Y
 * temp2 is attributes
 */
void sprite_draw(void);

/**
 * Renders a block from chr. calls sprite_render in a loop
 * based on tempA (tile width) and tempB (tile height).
 * For now assumes all have same attributes
 */
void sprite_drawBlock(void);
void sprite_drawBlockFlipped(void);

void sprite_clearRemainingSprites(void);
void sprite_renderSprites(void);

void sprite_sprite0Wait(void);
void sprite_sprite0Render(void);
void sprite_sprite0Update(void);

void __fastcall__ sprite_debugU8(u8 val);

/**
 * Pass in the u8, but x and y in ci/cj)
 * ci is position of RIGHT nibble
 */
void sprite_renderU8(u8 val);


extern u8 sprite_sprite0Skip;
