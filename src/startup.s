.include "common.inc"

.export _startUp
.import _ppu_screenOff
.import _sprite_oamIndex
.import FamiToneInit
.import FamiToneMusicPlay
.import song2_music_data

  
.proc _startUp
  jsr _ppu_screenOff

  ;clear sprites
  ldx #0
  sta _sprite_oamIndex
  txa
  and #%11111100
  tax
  lda #$FF 
:
  sta OAM,x
  inx
  inx
  inx
  inx
  bne :-

  ;clear nametables
  lda #$23
  sta PPUADDR
  lda #$C0
  sta PPUADDR
  ldx #255
:
  stx PPUDATA
  dex
  bpl :-

  lda #$27
  sta PPUADDR
  lda #$C0
  sta PPUADDR
  ldx #255
:
  stx PPUDATA
  dex
  bpl :-

  lda #$2B
  sta PPUADDR
  lda #$C0
  sta PPUADDR
  ldx #255
:
  stx PPUDATA
  dex
  bpl :-

  lda #$2F
  sta PPUADDR
  lda #$C0
  sta PPUADDR
  ldx #255
:
  stx PPUDATA
  dex
  bpl :-


  lda #1;NTSC
  ldx #<song2_music_data
  ldy #>song2_music_data
  jsr FamiToneInit

  lda #0
  jsr FamiToneMusicPlay

  rts

.endproc


