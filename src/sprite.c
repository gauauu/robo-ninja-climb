#include "common.h"
#include "sprite.h"

u8 sprite_sprite0Skip;

u16 sprite_sprite0Y;
s16 sprite_sprite0Dir;

void sprite_drawBlock(void) {
  //save width
  tempC = tempA;
  //save original X position
  tempD = ci;
  while (tempB > 0) {
    tempA = tempC;
    while (tempA > 0) {
      sprite_draw();
      --tempA;
      ci += 8;
      temp += 1;
    }
    //move destination to next row
    ci = tempD;
    cj += 8;
    //move chr index to next row
    temp -= tempC;
    temp += 16;

    //decrease counter
    --tempB;
  }
}

void sprite_drawBlockFlipped(void) {
  //save width
  tempC = tempA;
  //save original tile
  tempD = temp;

  //save original X position at right
  tempD = ci + (tempC * 8);

  //start with right-most
  ci = tempD;

  temp2 |= FLIP_HORIZ;

  while (tempB > 0) {
    tempA = tempC;
    while (tempA > 0) {
      sprite_draw();
      --tempA;
      ci -= 8;
      temp += 1;
    }
    //move destination to next row
    ci = tempD;
    cj += 8;
    //move chr index to next row
    temp -= tempC;
    temp += 16;

    //decrease counter
    --tempB;
  }
}

void sprite_init(void) {
    sprite_sprite0Y = (200 << 8);
    sprite_sprite0Dir = -100;
}

void sprite_sprite0Update(void) {
  if (MSB(sprite_sprite0Y) > 200) {
    sprite_sprite0Dir = -100;
  } 
  if (MSB(sprite_sprite0Y) < 190) {
    sprite_sprite0Dir = 100;
  }
  sprite_sprite0Y += sprite_sprite0Dir;
  
}
