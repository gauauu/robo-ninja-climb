#include "common.h"

extern u8 mc_status;
extern u8 mc_renderY;

extern u16 mc_x;
extern u16 mc_y;

void mc_update(void);
void mc_render(void);

void mc_dead(void);


void mc_init(void);

extern u8 mc_blockYMin;
extern u8 mc_blockYMax;

extern u8 mc_collideRight;
extern u8 mc_collideLeft;

extern u8 mc_aReleased;

extern u8 mc_boundaryInverted;
extern u8 mc_health;
extern u8 mc_maxHealth;

//gets auto-set to 0 on startUp()
extern u16 mc_deaths;

#define MC_STATUS_CLING_R    0
#define MC_STATUS_CLING_L    1
#define MC_STATUS_ASCENDING  2
#define MC_STATUS_DESCENDING 3
#define MC_STATUS_DEAD       4



#define MC_COL_HEIGHT 38
#define MC_COL_WIDTH 30

