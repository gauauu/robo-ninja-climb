#include "types.h"
#include "items.h"
#include "controller.h"
#include "common.h"
#include "ppu.h"
#include "sprite.h"
#include "mc.h"
#include "bg.h"
#include "misc.h"
#include "obstacle.h"
#include "title.h"
#include "dead.h"
#include "level.h"
#include "text.h"

#define LEVEL_SCREEN_TIMER (350)
#define LEVEL_SCREEN_ALLOW_SKIP (LEVEL_SCREEN_TIMER - 60)

#define LEVEL_START 0
#define LEVEL_VICTORY 5


u8 level_current;
u8 level_index;

u8 level_complete;
u16 level_screenTimer;

extern u8 level0_max;
extern u8 level0test_max;
extern u8 level0;
extern u8 level0test;

u8 * level_rowPtr;

u8 level_upcoming;

extern u8 firstTime;

extern u8* level_mapsLo[];
extern u8* level_mapsHi[];

extern u8* level_maxesLo[];
extern u8* level_maxesHi[];

extern u8* level_text1PointersLo[];
extern u8* level_text1PointersHi[];

extern u8* level_text2PointersLo[];
extern u8* level_text2PointersHi[];

extern u8* level_inst0PointersLo[];
extern u8* level_inst0PointersHi[];

extern u8* level_inst1PointersLo[];
extern u8* level_inst1PointersHi[];



void level_restartLevel(void) {
  LOAD_PTR_FROM_ARRAYS(level_rowPtr, level_mapsLo, level_mapsHi, level_current);
  LOAD_PTR_FROM_ARRAYS(tempPtr, level_maxesLo, level_maxesHi, level_current);
  level_index = *tempPtr;
}

void level_init(void){
  level_current = LEVEL_START;
  level_complete = true;
  LOAD_PTR_FROM_ARRAYS(level_rowPtr, level_mapsLo, level_mapsHi, level_current);
  LOAD_PTR_FROM_ARRAYS(tempPtr, level_maxesLo, level_maxesHi, level_current);
  level_index = *tempPtr;
}


void level_setComplete(){
  level_complete = true;
  ++level_current;
  LOAD_PTR_FROM_ARRAYS(level_rowPtr, level_mapsLo, level_mapsHi, level_current);
  LOAD_PTR_FROM_ARRAYS(tempPtr, level_maxesLo, level_maxesHi, level_current);
  level_index = *tempPtr;
}

void level_getNextRow(void){
  temp = level_rowPtr[level_index];
#ifdef CHEAT_B_SKIP
  if (ctrl_current & PAD_B) {
    level_index = 0;
  }
#endif
  if (level_index == 0) {
    level_setComplete();
  } else {
    --level_index;
  }
  level_upcoming = temp;
}


void level_setLevel(int level) {
  level_current = level;
  //do some sort of magic to set the level pointer.
  //probalby just hard code it?
  
}


extern u8 text_level0;
extern u8 text_level0title;
extern u8 text_level0inst0;
extern u8 text_level0inst1;

void level_screenSetup(void);


void level_showRoboNinja(void) {
  ci = 108;
  cj = 88;

  temp = title_animFrame;
  if (temp == 4) {
    temp = 2;
  }
  if (temp == 5) {
    temp = 1;
  }
  temp = temp * 4;

  temp2 = 0;
  tempA = 4;
  tempB = 5;
  sprite_drawBlock();
}


extern u8 text_level0inst2;
extern u8 text_level0inst3;

void level_showLevelScreen(void) {
  
  if (level_current == LEVEL_VICTORY) {
    victory_run();
  }

  switch(level_current){
    case 0:
      bg_tileModifier = 0;
      break;
    case 1:
      bg_tileModifier = 1;
      break;
    default:
      bg_tileModifier = 0;
      break;
  }

  LOAD_PTR_FROM_ARRAYS(tempPtr,  level_text1PointersLo, level_text1PointersHi, level_current);
  LOAD_PTR_FROM_ARRAYS(tempPtr2, level_text2PointersLo, level_text2PointersHi, level_current);
  
  waitVBlank();

  level_screenSetup();
  level_complete = false;
  level_screenTimer = LEVEL_SCREEN_TIMER;

  if (level_current > 0) {
    cj = 19;
    LOAD_PTR_FROM_ARRAYS(tempPtr,  level_inst0PointersLo, level_inst0PointersHi, level_current);
    drawTextCenteredCj();

    cj = 21;
    LOAD_PTR_FROM_ARRAYS(tempPtr,  level_inst1PointersLo, level_inst1PointersHi, level_current);
    drawTextCenteredCj();
  }

  if (level_current == 0) {
    cj = 19;
    LOAD_PTR_FROM_ARRAYS(tempPtr,  level_inst0PointersLo, level_inst0PointersHi, level_current);
    drawTextCenteredCj();

    cj = 20;
    LOAD_PTR_FROM_ARRAYS(tempPtr,  level_inst1PointersLo, level_inst1PointersHi, level_current);
    drawTextCenteredCj();

    cj = 22;
    tempPtr = &text_level0inst2;
    drawTextCenteredCj();

    cj = 23;
    tempPtr = &text_level0inst3;
    drawTextCenteredCj();
  }
    

  title_animFrame = 0;
  title_animTimer = TITLE_ANIM_TIME;



  ppu_screenOnMain();
  //ppu_showLeftNametableAtYScroll();

  while(true) {
    title_allLoopStuff();
    level_showRoboNinja();
    sprite_clearRemainingSprites();

    if (level_current > 0) {
      --level_screenTimer;
    }

    if (level_screenTimer == 0) {
      break;
    }

    if (ctrl_new & PAD_B) {
      break;
    }

    if (ctrl_new & PAD_START) {
      break;
    }

    if ((ctrl_current & PAD_A) && (level_screenTimer < LEVEL_SCREEN_ALLOW_SKIP)){
      break;
    }


    --title_animTimer;
    if (title_animTimer == 0) {
      title_animTimer = TITLE_ANIM_TIME;
      ++title_animFrame;
      if (title_animFrame == 6) {
        title_animFrame = 0;
      }
    }



  }

  //get back to the other gfx bank
  title_tearDown();

  ppu_screenOff();
  //prep to go back to main game
  mc_init();
  bg_prepBackground();
  bg_init();
  items_init();
  obstacle_init();
  ppu_loadPalette(level_current);

  ppu_screenOnMain();
  sprite_sprite0Skip = true;

}
