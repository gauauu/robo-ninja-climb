.include "common.inc"

.segment "CODE"

.export _ppu_screenOff
.proc _ppu_screenOff
  lda #00
  sta PPUMASK
  rts
.endproc



.export _ppu_loadPalette
.proc _ppu_loadPalette
  ;a is palette number
  ; seek to the start of palette memory ($3F00-$3F1F)
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR

palSelector:
  ;levels 0,4
  cmp #4
  beq :+
  cmp #0
  bne :++
:
    lda bg2_palette,x
    sta PPUDATA
    inx
    cpx #32
    bcc :-
    rts
:


  ;level 1
  cmp #1
  bne :++
:
    lda initial_palette,x
    sta PPUDATA
    inx
    cpx #32
    bcc :-
    rts
:

  ;level 2
  cmp #2
  bne :++
:
    lda cave_palette,x
    sta PPUDATA
    inx
    cpx #32
    bcc :-
    rts
:

  ;level 3
  cmp #3
  bne :++
:
    lda pal_4,x
    sta PPUDATA
    inx
    cpx #32
    bcc :-
    rts
:


  ;if we didn't find one, use #0
  lda #0
  jmp palSelector

.endproc



.proc _ppu_screenOnMain
.export _ppu_screenOnMain
  lda #NT_2000
  ldx #0
  ldy #0
.endproc

  
.export _ppu_screenOn
; a - nametable to show (ie NT_2000)
; x - hscroll
; y - vscroll
.proc _ppu_screenOn
  stx PPUSCROLL
  sty PPUSCROLL
  ora #VBLANK_NMI|BG_0000|OBJ_1000
  sta PPUCTRL
  lda #BG_ON|OBJ_ON|BG_CLIP|OBJ_CLIP
  sta PPUMASK
  rts
.endproc


.export _ppu_showLeftNametableAtYScroll
.import _camera_y
.proc _ppu_showLeftNametableAtYScroll
  lda #NT_2000
  ldx #0
  ldy _camera_y+1
  jmp _ppu_screenOn
.endproc

.export _ppu_profileStart
.proc _ppu_profileStart
  lda #$19
  sta $2001
  rts
.endproc

.export _ppu_profileEnd
.proc _ppu_profileEnd
  lda #$18
  sta $2001
  rts
.endproc

.segment "RODATA"
.export sprite_palette
initial_palette:
  ;.byt $01, $11, $03, $1a
  ;.byt $01, $10, $2d, $00
  ;.byt $01, $2a, $1a, $2a
  ;.byt $01, $19, $1b, $28
  ;.byt $07,$00,$10,$30,$07,$01,$21,$31,$07,$06,$16,$26,$07,$09,$19,$29
  .incbin "data/bg1.pal"
sprite_palette:
  .byt $01, $0e, $0b, $29
  .byt $01, $0e, $08, $3A
  .byt $01, $0e, $08, $20
  .byt $01, $0e, $12, $37
  


.export bg2_palette
bg2_palette:
  .byt $0C, $2D, $03, $13
  .byt $0C, $20, $2D, $00
  .byt $0C, $08, $18, $38
  .byt $0C, $16, $06, $2F

  .byt $0C, $0e, $0b, $29
  .byt $0C, $0e, $08, $3A
  .byt $0C, $0e, $08, $20
  .byt $0C, $0e, $12, $37


.export cave_palette
cave_palette:
  .byt $2D, $00, $2D, $00
  .byt $2D, $18, $28, $38
  .byt $2D, $00, $10, $10
  .byt $2D, $16, $06, $34

  .byt $2D, $0e, $0b, $29
  .byt $2D, $0e, $08, $3A
  .byt $2D, $0e, $08, $20
  .byt $2D, $0e, $12, $37



.export pal_4
pal_4:
  .byt $0C, $2D, $03, $13
  .byt $0C, $00, $10, $00
  .byt $0C, $00, $18, $38
  .byt $0C, $16, $06, $2F

  .byt $0C, $0e, $0b, $29
  .byt $0C, $0e, $08, $3A
  .byt $0C, $0e, $08, $20
  .byt $0C, $0e, $12, $37
