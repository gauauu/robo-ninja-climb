#include "title.h"
#include "types.h"
#include "controller.h"
#include "common.h"
#include "ppu.h"
#include "sprite.h"
#include "dead.h"
#include "mc.h"
#include "bg.h"
#include "misc.h"
#include "text.h"

extern void startUp(void);
extern void FamiToneUpdate(void);

extern void title_setup(void);
extern void title_tearDown(void);

extern u8 text_nt3;

u16 title_timer;

u8 title_animFrame;
u8 title_animTimer;



void title_allLoopStuff() {

    waitVBlank();
    sprite_renderSprites();
    ppu_screenOnMain();
    //ppu_showLeftNametableAtYScroll();

    //do stuff to keep things acting nice
    FamiToneUpdate();
    randomU8();
    ctrl_update();

}

void title_showRoboNinja(void) {
  ci = 108;
  cj = 13;

  temp = title_animFrame;
  if (temp == 4) {
    temp = 2;
  }
  if (temp == 5) {
    temp = 1;
  }
  temp = temp * 4;

  temp2 = 0;
  tempA = 4;
  tempB = 5;
  sprite_drawBlock();
}

extern void title_setupDifficultyMenu(void);


void title_renderDifficultySelector(void) {
  cj = 22 * 8 - 2;
  if (mc_maxHealth == 2) {
    cj = 20 * 8 - 2;
  }
  ci = 11 * 8;
  temp = SPRITE_TILE_SELECTOR;
  temp2 = 0;
  sprite_draw();

}


void title_switchDifficulty(void) {
  mc_maxHealth = 2 - mc_maxHealth;
}

void title_run(void) {
  title_setup();

  DRAW_TEXT(text_nt3, 8, 25);

  ppu_screenOnMain();
  ppu_showLeftNametableAtYScroll();

  title_timer = 0;
  title_animFrame = 0;
  title_animTimer = TITLE_ANIM_TIME;

  while(true) {
    title_allLoopStuff();
    title_showRoboNinja();
    sprite_clearRemainingSprites();
    ++title_timer;

    --title_animTimer;
    if (title_animTimer == 0) {
      title_animTimer = TITLE_ANIM_TIME;
      ++title_animFrame;
      if (title_animFrame == 6) {
        title_animFrame = 0;
      }
    }


    if (ctrl_new & PAD_A || title_timer > TITLE_CREDIT_TIME) {
      credits_run();
      title_timer = 0;
    }

    if (ctrl_current & PAD_START) {
      break;
    }

  }


  //////when start pressed, switch to the difficulty menu
  title_setupDifficultyMenu();
  while(true) {
    title_allLoopStuff();
    title_renderDifficultySelector();
    title_showRoboNinja();
    sprite_clearRemainingSprites();
    ++title_timer;

    --title_animTimer;
    if (title_animTimer == 0) {
      title_animTimer = TITLE_ANIM_TIME;
      ++title_animFrame;
      if (title_animFrame == 6) {
        title_animFrame = 0;
      }
    }


    if ((ctrl_new & PAD_A) || (ctrl_new & PAD_START)){
      break;
    }

    if ((ctrl_new & PAD_UP) || 
        (ctrl_new & PAD_DOWN) ||
        (ctrl_new & PAD_SELECT)) {
        title_switchDifficulty();
    }

  }


  title_tearDown();

}


void credits_setup(void);
void credits_run(void) {
  credits_setup();
  ppu_screenOnMain();
  ppu_showLeftNametableAtYScroll();

  title_timer = 0;
  while(true) {
    title_allLoopStuff();
    sprite_clearRemainingSprites();
    ++title_timer;

    if (ctrl_new & PAD_A || title_timer > TITLE_CREDIT_TIME) {
      break;
    }


    if (ctrl_current & PAD_START) {
      break;
    }

  }

  title_tearDown();
  title_setup();

}

void victory_drawRoboNinja(void) {
}

extern void start(void);
void victory_setup(void);
void victory_run(void) {

  dead_loadDeathTextBuffer();


  victory_setup();
  ppu_screenOnMain();
  ppu_showLeftNametableAtYScroll();

  title_timer = 0;
  while(true) {
    title_allLoopStuff();
    victory_drawRoboNinja();
    sprite_clearRemainingSprites();
    ++title_timer;

    if (ctrl_new & PAD_START) {
      break;
    }

  }

  title_tearDown();
  title_setup();
  start();

}
