;
; NES I/O definitions
; Copyright 2010 Damian Yerrick
; modified 2016 by Nathan Tolbert
;
; Copying and distribution of this file, with or without
; modification, are permitted in any medium without royalty provided
; the copyright notice and this notice are preserved in all source
; code copies.  This file is offered as-is, without any warranty.
;

PPUCTRL = $2000
NT_2000 = $00
NT_2400 = $01
NT_2800 = $02
NT_2C00 = $03
VRAM_DOWN = $04
OBJ_0000 = $00
OBJ_1000 = $08
OBJ_8X16 = $20
BG_0000 = $00
BG_1000 = $10
VBLANK_NMI = $80

PPUMASK = $2001
LIGHTGRAY = $01
BG_OFF = $00
BG_CLIP = $08
BG_ON = $0A
OBJ_OFF = $00
OBJ_CLIP = $10
OBJ_ON = $14
TINT_R = $20
TINT_G = $40
TINT_B = $80

OAM = $0200

PPUSTATUS = $2002
OAMADDR = $2003
; Don't worry about $2004; let OAM_DMA do the work for you.
PPUSCROLL = $2005
PPUADDR = $2006
PPUDATA = $2007

OAM_DMA = $4014
SNDCHN = $4015
P1 = $4016
P2 = $4017

KEY_A      = %10000000
KEY_B      = %01000000
KEY_SELECT = %00100000
KEY_START  = %00010000
KEY_UP     = %00001000
KEY_DOWN   = %00000100
KEY_LEFT   = %00000010
KEY_RIGHT  = %00000001

PAL_BASE    = $3F
PAL_BG      = $00
PAL_OBJ     = $10
COLOR_BLACK = $0F
COLOR_WHITE = $30

SCREEN_LEFT = $20
SCREEN_RIGHT = $24

.importzp _ci
.importzp _cj
.importzp _temp
.importzp _temp2



.macro drawString textLabel,ppuhi,ppulo
  lda #<textLabel
  sta _tempPtr
  lda #>textLabel
  sta _tempPtr+1
  lda #ppulo
  ldx #ppuhi
  jsr _drawText
.endmacro


;oops, the first doens't allow indexed,
;but I don't want to refactor it everywhere
; this is the same things as drawString
;but doesn't assume ppulo and ppuhi are absolute
;addressed, allows immediate addressed
.macro drawStringImmediate textLabel,ppuhi,ppulo
  lda #<textLabel
  sta _tempPtr
  lda #>textLabel
  sta _tempPtr+1
  lda ppulo
  ldx ppuhi
  jsr _drawText
.endmacro

.macro centeredText yPos,textAddr
  lda #<textAddr
  sta _tempPtr
  lda #>textAddr
  sta _tempPtr+1
  ldx #>($2000 + yPos * 32)
  lda #<($2000 + yPos * 32)
  jsr _drawTextCentered
.endmacro

.macro centeredTextTempPtr yPos
  ldx #>($2000 + yPos * 32)
  lda #<($2000 + yPos * 32)
  jsr _drawTextCentered
.endmacro

.macro centeredTextTempPtr2 yPos
  lda _tempPtr2
  sta _tempPtr
  lda _tempPtr2+1
  sta _tempPtr+1
  ldx #>($2000 + yPos * 32)
  lda #<($2000 + yPos * 32)
  jsr _drawTextCentered
.endmacro
