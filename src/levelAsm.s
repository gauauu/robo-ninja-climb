.include "common.inc"

.import _startUp
.import title_nt
.import title_pal
.import sprite_palette
.import _ppu_screenOff
.import _sprite_oamIndex
.import FamiToneInit
.import FamiToneMusicPlay
.import song2_music_data
.import _drawTextCentered
.import _drawText
.importzp _tempPtr
.importzp _tempPtr2
.import _text_nt0



.import _text_nt0
.import _text_nt1
.import _text_nt2
.import _text_nt3

.import _text_bg
.import _text_bgurl
.import _text_bglic
.import _text_bgpath

.import _text_font

.import _text_rn
.import _text_rnchris
.import _text_pressstart


.export _level_screenSetup
.proc _level_screenSetup
  jsr _ppu_screenOff

  ;load title palette
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR
copypalloop:
  lda title_pal,x
  sta PPUDATA
  inx
  cpx #16
  bcc copypalloop


  ;load roboninja-palette
  ;but don't bg color
  lda #$3f
  sta PPUDATA
  lda sprite_palette+1
  sta PPUDATA
  lda sprite_palette+2
  sta PPUDATA
  lda sprite_palette+3
  sta PPUDATA



  ;select 2nd chr bank
level_bankSwitch:
  lda #01
  sta level_bankSwitch+1


  ;load title screen and nametables
  lda #$20
  sta PPUADDR
  lda #$00
  sta PPUADDR

  ldy #30
  lda #$FF
:
  ldx #32
:
    sta PPUDATA
    dex
    bne :-
  dey
  bne :--

  lda #$21
  sta PPUADDR
  lda #$40
  sta PPUADDR

  ldy #8
  lda #$00
:
  ldx #32
:
    sta PPUDATA
    dex
    bne :-
  dey
  bne :--

  ;clear title attributes
  lda #$23
  sta PPUADDR
  lda #$C0
  sta PPUADDR
  ldx #64
  lda #0
:
  sta PPUDATA
  dex
  bne :-
  


  

  centeredTextTempPtr 6
  centeredTextTempPtr2 8


  rts
.endproc



