#include "common.h"

#define ITEM_DOUBLE_JUMP 1
#define ITEM_PAUSE_JUMP 2
#define ITEM_ROCKET 3

void items_init(void);
void items_spawnForLevel(void);
void items_spawn(void);
void items_update(void);
void items_render(void);

extern u8 items_owned[];

