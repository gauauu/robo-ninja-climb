#include "dead.h"
#include "sprite.h"
#include "ppu.h"
#include "sprite.h"
#include "mc.h"
#include "bg.h"
#include "misc.h"
#include "text.h"
#include "controller.h"
#include "title.h"
#include "items.h"
#include "level.h"
#include "obstacle.h"

#define DEAD_VELOCITY  0x0100
#define DEAD_TIME      60

#define DEAD_GO_CONTINUE 0
#define DEAD_GO_END      1

#define DEAD_ANIM_TIME 7

#define RENDER_PART(x,y,tile) ci = MSB(dead_##x); \
                              cj = MSB(dead_##y); \
                              temp = (tile); \
                              sprite_draw();\

extern u8 text_dontgiveup;
extern u8 text_tryagain;
extern u8 text_deaths2;
extern u8 text_continue;
extern u8 text_end;


u8 dead_deathBuffer[] = {0,0,0,0,0};


u8 dead_timer;

u16 dead_sourceY;
u16 dead_sourceX;

u16 dead_minusX;
u16 dead_plusX;
u16 dead_minusY;
u16 dead_plusY;

u8 dead_gameOverSelected;

extern void start(void);

void dead_begin(void) {
  dead_sourceY = (mc_renderY + 24) << 8;
  dead_sourceX = mc_x + (24 << 8);

  dead_minusX = dead_sourceX;
  dead_plusX = dead_sourceX;

  dead_minusY = dead_sourceY;
  dead_plusY = dead_sourceY;
  dead_timer = 0;
}

void dead_gameOverScreenSetup(void);

void dead_renderGameOverSelector(void) {
  cj = 22 * 8 - 2;
  if (dead_gameOverSelected == DEAD_GO_CONTINUE) {
    cj = 20 * 8 - 2;
  }
  ci = 10 * 8;
  temp = SPRITE_TILE_SELECTOR;
  temp2 = 0;
  sprite_draw();

}

extern u8 dead_frames;


void dead_loadDeathTextBuffer(void) {
  dead_deathBuffer[0] = 4;
  dead_deathBuffer[4] = (MSB(mc_deaths) >> 4) + 0xA0;
  dead_deathBuffer[3] = (MSB(mc_deaths) & 0x0F) + 0xA0;
  dead_deathBuffer[2] = (LSB(mc_deaths) >> 4) + 0xA0;
  dead_deathBuffer[1] = (LSB(mc_deaths) & 0x0F) + 0xA0;
}


void dead_drawRoboNinja(void) {
  ci = 108;
  cj = 100;

  tempPtr = &(dead_frames);
  temp = title_animFrame;
  if (temp > 17) {
    temp = 35 - temp;
  }
  temp = tempPtr[temp] * 5 + 0x60;
  temp2 = 0;
  tempA = 5;
  tempB = 3;

  sprite_drawBlock();
}

void dead_gameOverScreen(void) {

  waitVBlank();
  dead_gameOverScreenSetup();

  dead_loadDeathTextBuffer();

  DRAW_TEXT_CENTERED(text_dontgiveup, 4);
  DRAW_TEXT_CENTERED(text_tryagain, 6);
  DRAW_TEXT(text_deaths2,10, 8);
  DRAW_TEXT(dead_deathBuffer[0],18, 8);

  DRAW_TEXT(text_continue, 12, 20);
  DRAW_TEXT(text_end, 12, 22);

  title_animFrame = 0;
  title_animTimer = DEAD_ANIM_TIME;



  dead_gameOverSelected = DEAD_GO_CONTINUE;
  ppu_screenOnMain();
  while (true) {
    title_allLoopStuff();
    dead_renderGameOverSelector();
    dead_drawRoboNinja();
    sprite_clearRemainingSprites();

    if (ctrl_current & PAD_DOWN) {
      dead_gameOverSelected = DEAD_GO_END;
    }

    if (ctrl_current & PAD_UP) {
      dead_gameOverSelected = DEAD_GO_CONTINUE;
    }

    if (ctrl_new & PAD_SELECT) {
      dead_gameOverSelected = 1 - (dead_gameOverSelected);
    }

    if (ctrl_new & PAD_START) {
      break;
    }

    if (ctrl_new & PAD_A) {
      break;
    }

    
    --title_animTimer;
    if (title_animTimer == 0) {
      title_animTimer = DEAD_ANIM_TIME;
      ++title_animFrame;
      if (title_animFrame == 35) {
        title_animFrame = 0;
      }
    }



    
  }

  title_tearDown();
  if (dead_gameOverSelected == DEAD_GO_END) {
    start();
  }

  ppu_screenOff();

  //prep to go back to main game
  mc_init();
  bg_prepBackground();
  bg_init();
  items_init();
  obstacle_init();
  level_restartLevel();
  ppu_loadPalette(level_current);

  ppu_screenOnMain();
  sprite_sprite0Skip = 3;
  sprite_sprite0Update();

  sprite_sprite0Render();

}

void dead_update(void) {

  ++dead_timer;

  //this resets EVERYTHING
  if (dead_timer > DEAD_TIME) {
    //show game over screen
    dead_gameOverScreen();
    return;
    //start();
  }

  //calc positions
  //dead_halfPos += (DEAD_VELOCITY / 2);

  dead_minusX += DEAD_VELOCITY;
  dead_plusX -= DEAD_VELOCITY;

  dead_minusY -= DEAD_VELOCITY;
  dead_plusY += DEAD_VELOCITY;

}

void dead_renderPart(void) {
  sprite_draw();
}

void dead_render(void) {


  temp2 = 0;

  RENDER_PART(sourceX, minusY, 3);
  RENDER_PART(plusX, minusY, 5);
  RENDER_PART(plusX, sourceY, 21);
  RENDER_PART(plusX, plusY, 37);
  RENDER_PART(sourceX, plusY, 35);
  RENDER_PART(minusX, plusY, 33);
  RENDER_PART(minusX, sourceY, 16);
  RENDER_PART(minusX, minusY, 2);

}
