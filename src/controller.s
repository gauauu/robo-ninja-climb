
    .export _ctrl_update

    .export _ctrl_current
    .export _ctrl_new

.include "global.inc"

.segment "BSS"
_ctrl_current:  .res 1
_ctrl_new:      .res 1


.segment "CODE"


CTRL1  = $4016

.proc breakThings
    lda #20
    sta PPUADDR
    lda #00
    sta PPUADDR
:
    ldx #0
    stx PPUDATA
    dex
    jmp :-
.endproc


.proc _ctrl_update

buffer = 0 ; in theory, we might want to do a double-check read.
           ; but won't bother unless we use 

  ;prep reading the controller port
  ldx #1
  stx CTRL1
  ldx #0
  stx CTRL1

  ;currently we only care about player 1 for this game

  ldy #8
  ;loop through and read all buttons
:
  lda CTRL1
  and #%00000011
  cmp #1
  rol buffer
  dey
  bne :-

  ;now compare to old values to see what is pressed this frame
  lda _ctrl_current
  eor #$FF
  and buffer
  sta _ctrl_new

  lda buffer
  sta _ctrl_current

  rts
.endproc
