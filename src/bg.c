#include "bg.h"
#include "controller.h"


u16 camera_y;
u16 camera_vy;
u16 camera_screen;


u8 bg_offscreenRow;
u16 bg_offscreenRowAddress;
u16 bg_offscreenAttrAddress;

u8 bg_renderQueue[128];
u8 bg_renderIndex;


u8 bg_attrRowToRender;
u8 bg_attrMirror[64];

u8 bg_tileModifier;

#define BG_ATTR_SKIP 255


void bg_calcOffscreenRowAddress(void){
  //first calc screen row number
  //that's offscreen
  bg_offscreenRow = MSB(camera_y) - 16;
  if (bg_offscreenRow > 240) {
    bg_offscreenRow -= 16;
  }
  bg_offscreenRow /= 8;

  //translate that to an address
  bg_offscreenRowAddress = 0x2000 + bg_offscreenRow * 32;
}

void bg_update(void) {
  camera_y -= camera_vy;
  if (MSB(camera_y) > 240) {
    camera_y -= (16 << 8);
    camera_screen++;
  }
  bg_calcOffscreenRowAddress();
  bg_renderIndex = 0;
  bg_attrRowToRender = BG_ATTR_SKIP;

#ifdef CHEAT_SEL_PAUSE
  if (ctrl_current & PAD_SELECT) {
    camera_vy = 0;
  }
#endif
}

void bg_render(void) {
}

void bg_init(void) {
    camera_y = 0;
    camera_vy = 150;
#ifdef CHEAT_NOSCROLL
    camera_vy = 0;
#endif
    camera_screen = 0;

    bg_renderIndex = 0;
    bg_attrRowToRender = BG_ATTR_SKIP;
}
