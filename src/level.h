#include "common.h"


extern u8 level_current;
extern u8 level_complete;
extern u8 level_index;

void level_restartLevel(void);
void level_init(void);
void level_getNextRow(void);
void level_setLevel(int level);

void level_showLevelScreen(void);
