.include "global.inc"
.import temple_nt

.import _bg_tileModifier
.import _level_current

.segment "RODATA"

bg_level_tilesOffsets:
  .byt $40, $0, $60, $80, $40

.segment "CODE"
.export _bg_prepBackground
.proc _bg_prepBackground

  ldx _level_current
  lda bg_level_tilesOffsets,x
  sta _bg_tileModifier

  clc

  ;default PPU ctrl might be ok for horiz draw
  lda #$20
  sta PPUADDR
  lda #$00
  sta PPUADDR


  ldx #0
:
  lda temple_nt,x
  adc _bg_tileModifier
  sta PPUDATA
  inx
  bne :-

  ldx #0
:
  lda temple_nt+256,x
  adc _bg_tileModifier
  sta PPUDATA
  inx
  bne :-

  ldx #0
:
  lda temple_nt+512,x
  adc _bg_tileModifier
  sta PPUDATA
  inx
  bne :-

  ldx #0
:
  lda temple_nt+768,x
  adc _bg_tileModifier
  sta PPUDATA
  inx
  bne :-

  ;re-do attributes without tile modifier
  lda #$23
  sta PPUADDR
  lda #$C0
  sta PPUADDR

  ldx #(256-64)
:
  lda temple_nt+768,x
  sta PPUDATA
  inx
  bne :-

  ;now draw spikes on right nametable
  lda #$27
  sta PPUADDR
  lda #$80
  sta PPUADDR

  lda #$0f
  ldx #32
:
  sta PPUDATA
  dex
  bne :-

  lda #$1f
  ldx #32
:
  sta PPUDATA
  dex
  bne :-

;now filler on the top
  lda #$24
  ldx #$00
  sta PPUADDR
  stx PPUADDR
  
  lda #5
  ldx #64
:
  sta PPUDATA
  dex
  bpl :-

;attributes for filler
  lda #$27
  ldx #$C0
  sta PPUADDR
  stx PPUADDR
  lda #%01010101
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA
  sta PPUDATA


  ;attributes for spikes
  lda #$27
  sta PPUADDR
  lda #$F8
  sta PPUADDR

  lda #$55
  ldx #8
:
  sta PPUDATA
  dex
  bne :-

  ;initialize the attr mirror
  ldx #63
:
  lda temple_nt+$03C0,x
  sta _bg_attrMirror,x
  dex
  bpl :-


  rts

.endproc

bg_attrMasks:
  .byt $F0
  .byt $0F

.export _bg_loadAttrMirrorRow
.import _obstacle_evenOdd
.proc _bg_loadAttrMirrorRow

  ;set the appropriate mask in temp2
  ldx _obstacle_evenOdd
  bne bottom


  ldy #8
  ldx _temp
:
  lda temple_nt+$03C0,x
  and #$F0
  sta _temp
  lda _bg_attrMirror,x
  and #$0F
  ora _temp
  sta _bg_attrMirror,x
  inx
  dey
  bne :-
  rts

bottom:
  ldy #8
  ldx _temp
:
  lda temple_nt+$03C0,x
  and #$0F
  sta _temp
  lda _bg_attrMirror,x
  and #$F0
  ora _temp
  sta _bg_attrMirror,x
  inx
  dey
  bne :-
  rts

.endproc



;ridiculously unoptimized render
;queue which is good enough for a game
;like this that only renders a few
;tiles per frame
.export _bg_processRenderQueue

.import _bg_renderQueue
.import _bg_renderIndex
.importzp _temp
.proc   _bg_processRenderQueue

  ldx _bg_renderIndex;
  ;index is the first empty, so 
  ;if it's 0, skip anything
  beq renderQueueDone
  ;otherwise decrease by 1 
  ; to fix off-by-1 error
  ; and save to compare
  dex
  stx _temp

  ldx #0
renderQueueTop:
    lda _bg_renderQueue,x
    sta PPUADDR
    inx
    lda _bg_renderQueue,x
    sta PPUADDR
    inx
    ldy _bg_renderQueue,x
:
      inx 
      lda _bg_renderQueue,x
      sta PPUDATA
      dey
      bne :-
    inx
    cpx _temp
    bne renderQueueTop


renderQueueDone:
  ldx  #0
  stx _bg_renderIndex;

  jmp _bg_processAttrQueue
.endproc


;assumptions: the attr queue will be blank
;or will have exactly 6 entries.
;the 3rd byte (after address) will have
;mask to indicate what should be masked
;on the attribute byte before ANDing the 
;new value

.import _bg_attrMirror
.import _bg_attrRowToRender
.importzp _temp
.importzp _temp2
.proc _bg_processAttrQueue

  ldx _bg_attrRowToRender
  cpx #255
  beq attrQueueDone


attrQueueTop:
    ;multiply by 8 to get offset into attr mirror
    ;and save it in x as in index
    lda _bg_attrRowToRender
    clc
    asl
    asl
    asl
    tax

    ;add to $23C0 to get attr address
    adc #$C0
    tay
    lda #0
    adc #$23

    sta PPUADDR
    sty PPUADDR


    ldy #8
:
      lda _bg_attrMirror,x
      sta PPUDATA
      inx
      dey
      bne :-

attrQueueDone:
  ldx  #255
  stx _bg_attrRowToRender
  rts

.endproc
