;
; Startup code for cc65 (NES version)
;
; by Groepaz/Hitmen <groepaz@gmx.net>
; based on code by Ullrich von Bassewitz <uz@cc65.org>
;

        .export         start
        .export         nmi_handler
        .export         irq_handler
        .export         start
        .export         _exit
        .export         __STARTUP__ : absolute = 1      ; Mark as startup
        .importzp       _VBLANK_FLAG

        .import         initlib, donelib, callmain
        .import         push0, _main, zerobss, copydata
        .import         ppubuf_flush

        .importzp      _timer

        ; Linker generated symbols
        .import         __STACK_START__, __STACK_SIZE__
        .import         __ROM0_START__, __ROM0_SIZE__
        .import         __STARTUP_LOAD__,__STARTUP_RUN__, __STARTUP_SIZE__
        .import         __CODE_LOAD__,__CODE_RUN__, __CODE_SIZE__
        .import         __RODATA_LOAD__,__RODATA_RUN__, __RODATA_SIZE__

        .include        "zeropage.inc"
        .include        "nes.inc"

        .export         _start

; ------------------------------------------------------------------------
; Place the startup code in a special segment.

.segment        "STARTUP"

_start:
start:

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;first take some precautions and clear everything
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  sei             ;disable interrupts
  ldx #$00
  stx PPU_CTRL1     ;disable all the ppu stuff
  stx PPU_CTRL2     ;
  stx $4010       ;
  ldx #$FF        ;set stack pointer
  txs             

  ;read registers to clear them
  lda APU_CHANCTRL
  lda PPU_STATUS   


  ;clear ram
  ldx #0
  lda #0
:
    sta $0000,x
    sta $0100,x
    sta $0200,x
    sta $0300,x
    sta $0400,x
    sta $0500,x
    sta $0600,x
    sta $0700,x
  dex
  bne :-





  ;sound
  lda #$40
  sta APU_PAD2          ; disable APU IRQ
  lda #$0F
  sta APU_CHANCTRL      

:
  bit PPU_STATUS   ;wait for first frame vblank
  bpl :-
  cld

  ;clear the zero page
  txa
:
  sta $00,x
  inx
  bne :-
  

  bit PPU_STATUS
:
  bit PPU_STATUS  ;second vblank, things should be stable after this
  bpl :-     
      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; now continue on with a modified version of cc65's crt0.s
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Set up the CPU and System-IRQ.

        sei
        cld
        ldx     #0
        stx     _VBLANK_FLAG

        ;stx     ringread
        ;stx     ringwrite
        ;stx     ringcount

        txs

        lda     #$20
;@l:     sta     ringbuff,x
        ;sta     ringbuff+$0100,x
        ;sta     ringbuff+$0200,x
        ;inx
        ;bne     @l

; Clear the BSS data.

        jsr     zerobss

; Initialize the data.
        ;jsr     copydata

; Set up the stack.

        lda     #<(__STACK_START__ + __STACK_SIZE__)
        ldx     #>(__STACK_START__ + __STACK_SIZE__)
        sta     sp
        stx     sp+1            ; Set argument stack ptr

; Call the module constructors.
        ;lda #bank
        ;sta $8000

        ;jsr     initlib

; Push the command-line arguments; and, call main().






        jsr     callmain

; Call the module destructors. This is also the exit() entry.

_exit:  ;jsr     donelib         ; Run module destructors

; Reset the NES.
        

        jmp start

; ------------------------------------------------------------------------
; System V-Blank Interrupt
; Updates PPU Memory (buffered).
; Updates VBLANK_FLAG and tickcount.
; ------------------------------------------------------------------------

nmi_handler:    pha
        tya
        pha
        txa
        pha

        ;lda     #1
        inc     _VBLANK_FLAG

        inc     tickcount
        bne     @s
        inc     tickcount+1

@s:     ;jsr     ppubuf_flush
        ;jsr vram_apply_last_buff

        ; Reset the video counter.
        ;lda     #$20
        ;sta     PPU_VRAM_ADDR2
        ;lda     #$00
        ;sta     PPU_VRAM_ADDR2

        ; Reset scrolling.
        ;sta     PPU_VRAM_ADDR1
        ;sta     PPU_VRAM_ADDR1
        inc _timer

        pla
        tax
        pla
        tay
        pla

; Interrupt exit

irq_handler:
        rti

; ------------------------------------------------------------------------
; hardware vectors
; ------------------------------------------------------------------------

.segment "VECTORS"

        .word   nmi_handler         ; $fffa vblank nmi
        .word   start       ; $fffc reset
        .word   irq_handler         ; $fffe irq / brk

; ------------------------------------------------------------------------
; character data
; ------------------------------------------------------------------------

;.segment "CHARS"

        ;.include        "neschar.inc"
