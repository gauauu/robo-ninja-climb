.include "common.inc"

.import _startUp
.import title_nt
.import title_pal
.import sprite_palette
.import _ppu_screenOff
.import _sprite_oamIndex
.import FamiToneInit
.import FamiToneMusicPlay
.import song2_music_data
.import _drawTextCentered
.import _drawText
.importzp _tempPtr
.import _text_nt0



.import _text_nt0
.import _text_nt1
.import _text_nt2
.import _text_nt3

.import _text_bg
.import _text_bgurl
.import _text_bglic
.import _text_bgpath

.import _text_font

.import _text_rn
.import _text_rnchris
.import _text_pressstart




.export _title_setup
.proc _title_setup
  ;select 2nd chr bank
  lda #01
  sta _title_setup+1


  ;clear everything, do startup
  ;jsr _startUp

  jsr _ppu_screenOff

  ;load title screen and nametables
  lda #$20
  sta PPUADDR
  lda #$00
  sta PPUADDR

  ldx #0
:
  lda title_nt,x
  sta PPUDATA
  inx
  bne :-

  ldx #0
:
  lda title_nt+256,x
  sta PPUDATA
  inx
  bne :-

  ldx #0
:
  lda title_nt+512,x
  sta PPUDATA
  inx
  bne :-

  ldx #0
:
  lda title_nt+768,x
  sta PPUDATA
  inx
  bne :-

  ;load title palette
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR
:
  lda title_pal,x
  sta PPUDATA
  inx
  cpx #16
  bcc :-

  ;load roboninja-palette
  ;but don't bg color
  lda #$3f
  sta PPUDATA
  lda sprite_palette+1
  sta PPUDATA
  lda sprite_palette+2
  sta PPUDATA
  lda sprite_palette+3
  sta PPUDATA





  rts
.endproc




.export _credits_setup
.proc _credits_setup
  jsr _ppu_screenOff

  ;select 2nd chr bank
  lda #01
  sta _title_setup+1


  ;load title screen and nametables
  lda #$20
  sta PPUADDR
  lda #$00
  sta PPUADDR

  ldy #30
  lda #$FF
:
  ldx #32
:
    sta PPUDATA
    dex
    bne :-
  dey
  bne :--

  ;clear title attributes
  lda #$23
  sta PPUADDR
  lda #$C0
  sta PPUADDR
  ldx #64
  lda #0
:
  sta PPUDATA
  dex
  bne :-
  


  ;load title palette
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR
copypalloop:
  lda title_pal,x
  sta PPUDATA
  inx
  cpx #32
  bcc copypalloop

  ;load roboninja-palette
  ;but don't bg color
  lda #$3f
  sta PPUDATA
  lda sprite_palette+1
  sta PPUDATA
  lda sprite_palette+2
  sta PPUDATA
  lda sprite_palette+3
  sta PPUDATA





  centeredText 4,_text_nt0
  centeredText 5,_text_nt1
  centeredText 6,_text_nt2
  centeredText 7,_text_nt3

  centeredText 9,_text_bg
  centeredText 10,_text_bgurl
  centeredText 11,_text_bgpath
  centeredText 12,_text_bglic

  centeredText 16,_text_font

  centeredText 18,_text_rn
  centeredText 19,_text_rnchris


  centeredText 23,_text_pressstart


  ;load text
  ;lda #<_text_nt0
  ;sta _tempPtr
  ;lda #>_text_nt0
  ;sta _tempPtr+1
  ;ldx #$20
  ;lda #$60
  ;jsr _drawTextCentered


  rts
.endproc



.export _title_tearDown
.proc _title_tearDown
  lda #0
  sta $8000
  rts
.endproc


.import _text_easy
.import _text_hard


.export _title_setupDifficultyMenu
.proc _title_setupDifficultyMenu

  jsr _ppu_screenOff

  ;erase bottom half of title
  lda #$22
  sta PPUADDR
  lda #$00
  sta PPUADDR

  ldy #10
  lda #$45
:
  ldx #32
:
  sta PPUDATA
  dex
  bpl :-
  dey 
  bpl :--

    
  ;fix attributes palettes
  lda #$23
  sta PPUADDR
  lda #$E5
  sta PPUADDR
  lda #0
  sta PPUDATA

  centeredText 20,_text_easy
  centeredText 22,_text_hard

  rts



.endproc

.import _text_congratulations
.import _text_youdidit
.import _text_youdidithard
.import _text_deaths
.import _text_nowtryhard
.import _sprite_renderU8
.import _mc_deaths
.import _mc_maxHealth
.import _dead_deathBuffer
.importzp _ci

.export _victory_setup
.proc _victory_setup

 ;select 2nd chr bank
  lda #01
  sta _victory_setup+1


  ;clear everything, do startup
  ;jsr _startUp

  jsr _ppu_screenOff

  ;load title screen and nametables
  lda #$20
  sta PPUADDR
  lda #$00
  sta PPUADDR

  ;first chunk -- use blank
  ldx #0
:
  lda title_nt
  sta PPUDATA
  inx
  bne :-

  ;second chunk - show normal title
  ldx #0
:
  lda title_nt+256,x
  sta PPUDATA
  inx
  bne :-

  ;and 3rd
  ldx #0
:
  lda title_nt+512
  sta PPUDATA
  inx
  bne :-

  ;4th chunk and attributes
  ldx #0
:
  lda title_nt+768,x
  sta PPUDATA
  inx
  bne :-

  ;load title palette
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR
:
  lda title_pal,x
  sta PPUDATA
  inx
  cpx #16
  bcc :-

  ;load roboninja-palette
  ;but don't bg color
  lda #$3f
  sta PPUDATA
  lda sprite_palette+1
  sta PPUDATA
  lda sprite_palette+2
  sta PPUDATA
  lda sprite_palette+3
  sta PPUDATA

  
  ;fix attributes palettes
  lda #$23
  sta PPUADDR
  lda #$E5
  sta PPUADDR
  lda #0
  sta PPUDATA

  ;remove the "climb"
  lda #$21
  sta PPUADDR
  lda #$0A
  sta PPUADDR
  ldx #12
  lda #0
:
  sta PPUDATA
  dex
  bpl :-
  lda #$21
  sta PPUADDR
  lda #$2A
  sta PPUADDR
  ldx #12
  lda #0
:
  sta PPUDATA
  dex
  bpl :-

  centeredText 17,_text_congratulations

  lda _mc_maxHealth
  beq :+
    centeredText 19,_text_youdidit
    centeredText 25,_text_nowtryhard
  jmp :++
:
    centeredText 19,_text_youdidithard
:
  centeredText 21,_dead_deathBuffer

  centeredText 23,_text_deaths

  rts
.endproc

