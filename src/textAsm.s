.include "global.inc"


;
;press start
;normal round
;battle
;speed
;danger
;survival
;narrow

;characters needed:
;eprstaoundmlbgviwy

LINE_BREAK = $ff


.importzp _temp
.importzp _temp2
.importzp _tempPtr
.importzp _ci
.importzp _cj
.importzp _tempU16

.segment "CODE"
;addr of text in _tempPtr
;A is lo ppu addr
;X is hi ppu addr
.export _drawText
.proc _drawText

  ;first update draw position
  stx PPUADDR
  sta PPUADDR

  ;start drawing
  ldy #0
  lda (_tempPtr),y
  tay
:
  lda (_tempPtr),y
  sta PPUDATA
  dey
  bne :-
  rts

.endproc

;addr of text line in _tempPtr
;A is lo ppu addr
;X is hi ppu addr
.export _drawTextLines
.proc _drawTextLines
tmplo = _temp
tmphi = _temp2

  sta tmplo
  stx tmphi

  stx PPUADDR
  sta PPUADDR

  ldy #0
  lda (_tempPtr),y
  tay
loop:

    lda (_tempPtr),y
    cmp #LINE_BREAK
    bne noLineBreak
      ;if line break,
      ;increment line
      lda tmplo
      clc
      adc #32
      sta tmplo
      tax
      lda tmphi
      adc #0
      sta tmphi
      ;and write it to PPUADDR
      sta PPUADDR
      stx PPUADDR
      jmp loopBottom

noLineBreak:
    sta PPUDATA
loopBottom:
  dey
  bne loop
  rts

.endproc


;addr of text line in _tempPtr
;A is lo ppu addr
;X is hi ppu addr
.export _drawTextCentered
.proc _drawTextCentered
tmplo = _temp
tmphi = _temp2

  ;store a and x for later
  sta tmplo
  stx tmphi

  ldy #0

  ;first see how long it is and
  ;center it
  lda #32               ;screen width
  sec
  sbc (_tempPtr),y 
  ;now a is amount of blank space
  ;total. divide by 2 for left side
  clc
  ror
  
  ;add it to the text address
  clc
  adc tmplo
  sta tmplo
  lda #0
  adc tmphi
  sta tmphi

  ;first update draw position
  lda tmphi
  sta PPUADDR
  lda tmplo
  sta PPUADDR

  ;start drawing
  ldy #0
  lda (_tempPtr),y
  tay
:
  lda (_tempPtr),y
  sta PPUDATA
  dey
  bne :-
  rts
.endproc


.export _drawTextCenteredCj
.proc _drawTextCenteredCj
;;;;cj * 32
  lda #0
  sta _tempU16+1
  lda _cj
  sta _tempU16

  ;2
  asl _tempU16
  rol _tempU16+1
  ;4
  asl _tempU16
  rol _tempU16+1
  ;8
  asl _tempU16
  rol _tempU16+1
  ;16
  asl _tempU16
  rol _tempU16+1
  ;32
  asl _tempU16
  rol _tempU16+1

;;;add $20 for address
  lda _tempU16+1
  clc
  adc #$20
  sta _tempU16+1
  tax

  lda _tempU16



  jmp _drawTextCentered

.endproc


.export _drawTextAtCiCj
.proc _drawTextAtCiCj
;;;;cj * 32
  lda #0
  sta _tempU16+1
  lda _cj
  sta _tempU16

  ;2
  asl _tempU16
  rol _tempU16+1
  ;4
  asl _tempU16
  rol _tempU16+1
  ;8
  asl _tempU16
  rol _tempU16+1
  ;16
  asl _tempU16
  rol _tempU16+1
  ;32
  asl _tempU16
  rol _tempU16+1

;;;add $20 for address
  lda _tempU16
  clc
  adc _ci
  sta _tempU16

  lda _tempU16+1
  adc #$20
  sta _tempU16+1
  tax

  lda _tempU16

  jmp _drawText

.endproc
