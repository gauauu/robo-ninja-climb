#include "obstacle.h"
#include "misc.h"
#include "bg.h"
#include "level.h"
#include "items.h"
#include "mc.h"
#include "controller.h"
#include "sprite.h"





u8 obstacle_x[OBSTACLE_COUNT];
u8 obstacle_y[OBSTACLE_COUNT];
u8 obstacle_type[OBSTACLE_COUNT];
u8 obstacle_state[OBSTACLE_COUNT];
u8 obstacle_screen[OBSTACLE_COUNT];
u8 obstacle_next;

u8 obstacle_framesSinceSpawn;

u8 obstacle_evenOdd;

u8 obstacle_thisRow;

extern u8 temple_nt;

#define OB_LEFT 48
#define OB_RIGHT 172

#define OB_TILE_LSPIKE 0x17
#define OB_TILE_RSPIKE 0x15
#define OB_TILE_MINE   0x1D

#define OB_TILE_LASER_LEFT    0x1B
#define OB_TILE_LASER_RIGHT   0x19
#define OB_TILE_LASERS        0x1A

#define NEW_OBSTACLE_DEPRECATED(XPOS, TYPE) obstacle_y[obstacle_next] = bg_offscreenRow * 8; \
                                 obstacle_x[obstacle_next] = (XPOS); \
                                 obstacle_type[obstacle_next] = OBSTACLE_TYPE_##TYPE; \
                                 ++obstacle_next; \
                                 bg_renderQueue[temp3] = OB_TILE_##TYPE; \
                                 ++temp3; \
                                 bg_renderQueue[temp3] = OB_TILE_##TYPE + 1; \
                                 ++temp3; \
                                 obstacle_checkMax();

#define NEW_OBSTACLE_DOUBLED(XPOS, TYPE) temp = (XPOS); \
                                 ci = OBSTACLE_TYPE_##TYPE; \
                                 cj = OB_TILE_##TYPE; \
                                 tempC = OB_TILE_##TYPE; \
                                 ob__newObstacle();

#define NEW_OBSTACLE_TALL(XPOS, TYPE) temp = (XPOS); \
                                 ci = OBSTACLE_TYPE_##TYPE; \
                                 cj = OB_TILE_##TYPE; \
                                 tempC = OB_TILE_##TYPE + 16; \
                                 ob__newObstacle();


#define OB_LASERS_ACTIVE  (timer & B01000000)

void obstacle_init(void) {
  for (ci = 0; ci <= OBSTACLE_MAX; ++ci){
    obstacle_type[ci] = OBSTACLE_TYPE_UNUSED;
  }
  obstacle_framesSinceSpawn = 0;
  obstacle_next = 0;
}

void obstacle_updateOne() {
  //y is cj
  cj = obstacle_y[ci];


  //figure out if it's new, or to be deleted
  if (cj == bg_offscreenRow) {
    //if it's at the offscreen row and no longer new, it's time to be deleted
    if (!(obstacle_state[ci] & OBSTACLE_FLAG_NEW)) {
      obstacle_state[ci] = OBSTACLE_FLAG_NONE;
      obstacle_type[ci] = OBSTACLE_TYPE_UNUSED;
      return;
    }
  } else {
    //if it's not at the offscreen row, mark it as no longer new
    obstacle_state[ci] &= ~OBSTACLE_FLAG_NEW;
  }

  //compare to player Y for collision
  if (mc_boundaryInverted) {
    tempB = (cj > mc_blockYMin || cj < mc_blockYMax);
  } else {
    tempB = (cj > mc_blockYMin && cj < mc_blockYMax);
  }
  
  if (tempB){

      if (obstacle_type[ci] == OBSTACLE_TYPE_LASERS) {
        if (OB_LASERS_ACTIVE) {
          mc_dead();
        }
        return;
      }

      temp = obstacle_x[ci];
      //check exact point for collisions
      if ((temp > mc_collideLeft) && 
          temp < mc_collideRight){

        mc_dead();
            
    }
  }
}


//x is temp
//type is ci
//tile is cj
//2nd row tile is tempC
void ob__newObstacle() {
  obstacle_y[obstacle_next] = bg_offscreenRow;
  obstacle_x[obstacle_next] = temp * 16 + OB_LEFT; 
  obstacle_type[obstacle_next] = ci; 
  obstacle_state[obstacle_next] = OBSTACLE_FLAG_NEW; 
  obstacle_screen[obstacle_next] = camera_screen;
  ++obstacle_next; 

  tempD = ci;

  if (ci == OBSTACLE_TYPE_LASERS) {
    ci = tempB - 2;
    bg_renderQueue[ci] = OB_TILE_LASER_LEFT;
    ++ci;
    bg_renderQueue[ci] = OB_TILE_LASER_LEFT+ 1;

    ci = tempB + 20;
    bg_renderQueue[ci] = OB_TILE_LASER_RIGHT;
    ++ci;
    bg_renderQueue[ci] = OB_TILE_LASER_RIGHT+ 1;

    ci = temp3 - 2;
    bg_renderQueue[ci] = OB_TILE_LASER_LEFT + 16;
    ++ci;
    bg_renderQueue[ci] = OB_TILE_LASER_LEFT + 17;

    ci = temp3 + 20;
    bg_renderQueue[ci] = OB_TILE_LASER_RIGHT + 16;
    ++ci;
    bg_renderQueue[ci] = OB_TILE_LASER_RIGHT + 17;

    return;

  }

  //now ci becomes the X offset in the renderQueue row
  ci = temp3 + (temp << 1);
  bg_renderQueue[ci] = tempC; //OB_TILE_##TYPE; 
  ++tempC;
  ++ci;
  bg_renderQueue[ci] = tempC; 

  ci = tempB + (temp << 1);
  bg_renderQueue[ci] = cj; //OB_TILE_##TYPE; 
  ++cj;
  ++ci;
  bg_renderQueue[ci] = cj; 


  //now put into attribute mirror
  tempD = (bg_attrRowToRender * 8) + ((temp + 1) / 2) + 1; //find attr cell
  tempC = bg_attrMirror[tempD]; //find current val
  if (temp & 1) {
    //we want the left
    if (obstacle_evenOdd) {
      //top-left
      tempC &= B11111100;
      tempC |= B00000011;
    } else {
      //bottom-left
      tempC &= B11001111;
      tempC |= B00110000;
    }
  } else {
    if (obstacle_evenOdd) {
      //top-right
      tempC &= B11110011;
      tempC |= B00001100;
    } else {
      //bottom-right
      tempC &= B00111111;
      tempC |= B11000000;
    }
  }
  bg_attrMirror[tempD] = tempC;


  if (obstacle_next > OBSTACLE_MAX) {
    obstacle_next = 0;
  }
}

void obstacle_spawnNew(void) {

  //first build array of "normal" row data
  //  //get offset into nametable data based on camera
  //then for each bit, add any obstacle tile




  //put in render queue
  tempU16 = bg_offscreenRowAddress;


  bg_renderQueue[bg_renderIndex] = MSB(tempU16);
  ++bg_renderIndex;
  bg_renderQueue[bg_renderIndex] = LSB(tempU16);
  ++bg_renderIndex;
  bg_renderQueue[bg_renderIndex] = 32;
  ++bg_renderIndex;

  //look up the "normal" row in rom
  tempPtr = &temple_nt + (bg_offscreenRow * 32);

  //temp3 holds the start of the renderIndex
  //for later when we overwrite values
  temp3 = bg_renderIndex + 6;

  //copy the normal row into the render queue


  ///////////////
  ///////////////
  // optimized version of below
  ///////////////
  ///////////////
  __asm__("lda #32");
  __asm__("sta %v", ci);
  __asm__("clc");
  __asm__("ldx %v", bg_renderIndex); //x = bg_renderIndex
  __asm__("ldy #0"); //y = temp
ob_loop1:
  __asm__("lda (%v),y", tempPtr);
  __asm__("adc %v", bg_tileModifier);
  __asm__("sta %v,x", bg_renderQueue);
  __asm__("iny");
  __asm__("inx");
  __asm__("dec %v", ci);
  __asm__("bne %g", ob_loop1);


  bg_renderIndex += 32;


  /*
   * PREVIOUS WAS OPTIMIZED VERSION OF THIS
   * temp2 = bg_renderIndex + 32;
   * for (temp = 0; bg_renderIndex < temp2; ++bg_renderIndex) {
   *  bg_renderQueue[bg_renderIndex] = tempPtr[temp];
   *  ++temp;
   *}
  */


  //calc next row also
  tempU16B = tempU16;
  tempU16B -= 32;
  /*
  if (tempU16B > 0x23BF) {
    tempU16B = 0x2000;
  }
  */
  if (tempU16B < 0x2000) {
    tempU16B = 0x23BF;
  }

  bg_renderQueue[bg_renderIndex] = MSB(tempU16B);
  ++bg_renderIndex;
  bg_renderQueue[bg_renderIndex] = LSB(tempU16B);
  ++bg_renderIndex;
  bg_renderQueue[bg_renderIndex] = 32;
  ++bg_renderIndex;

  //look up the "normal" row in rom
  /* NLT
  tempD = bg_offscreenRow + 1;
  if (tempD >= 30) {
    tempD = 0;
  }
  */
  tempD = bg_offscreenRow - 1;
  if (bg_offscreenRow == 0) {
    tempD = 29;
  }

  tempPtr = &temple_nt + (tempD * 32);

  //tempB holds the start of the renderIndex
  //for later when we overwrite values
  tempB = bg_renderIndex + 6;

  //copy the normal row into the render queue
  //
  ///////////////
  ///////////////
  // optimized version of below
  ///////////////
  ///////////////
  __asm__("lda #32");
  __asm__("sta %v", ci);
  __asm__("clc");
  __asm__("ldx %v", bg_renderIndex); //x = bg_renderIndex
  __asm__("ldy #0"); //y = temp
ob_loop2:
  __asm__("lda (%v),y", tempPtr);
  __asm__("adc %v", bg_tileModifier);
  __asm__("sta %v,x", bg_renderQueue);
  __asm__("iny");
  __asm__("inx");
  __asm__("dec %v", ci);
  __asm__("bne %g", ob_loop2);


  bg_renderIndex += 32;


  /*
   * OPTIMIZED FROM ABOVE
  temp2 = bg_renderIndex + 32;
  for (temp = 0; bg_renderIndex < temp2; ++bg_renderIndex) {
    bg_renderQueue[bg_renderIndex] = tempPtr[temp];
     ++temp;
  }
  */

  //set normal attr row into the attr mirror
  bg_attrRowToRender = bg_offscreenRow / 4;
  temp = ((bg_attrRowToRender) * 8);
  obstacle_evenOdd = ((bg_offscreenRow & 3) < 2);
  bg_loadAttrMirrorRow();


  
  //now start creating obstables
  level_getNextRow();

  obstacle_thisRow = temp;
    
  if (obstacle_thisRow == 0xFF) {
    //spawn item
    items_spawnForLevel();
  } else if (obstacle_thisRow == B01111110) {
    //fire shooter
    NEW_OBSTACLE_TALL(0, LASERS)

  } else {
    if (obstacle_thisRow & B10000000) {
      NEW_OBSTACLE_DOUBLED(0, LSPIKE)
    }
    if (obstacle_thisRow & B00000001) {
      NEW_OBSTACLE_DOUBLED(9, RSPIKE)
    }
    if (obstacle_thisRow & B01000000) {
      NEW_OBSTACLE_TALL(2, MINE)
    }
    if (obstacle_thisRow & B00100000) {
      NEW_OBSTACLE_TALL(3, MINE)
    }
    if (obstacle_thisRow & B00010000) {
      NEW_OBSTACLE_TALL(4, MINE)
    }
    if (obstacle_thisRow & B00001000) {
      NEW_OBSTACLE_TALL(5, MINE)
    }
    if (obstacle_thisRow & B00000100) {
      NEW_OBSTACLE_TALL(6, MINE)
    }
    if (obstacle_thisRow & B00000010) {
      NEW_OBSTACLE_TALL(7, MINE)
    }
  }

  ++bg_renderIndex;
  obstacle_framesSinceSpawn = 0;

}

u8 camera_lastMsbY;

void obstacle_updateAll(void) {
  ++obstacle_framesSinceSpawn;

  //see if we need to spawn anything
  temp = camera_lastMsbY ^ MSB(camera_y);
  if (temp & B00010000) {
    //TODO: make obstacle array bigger, force wrapping
    //instead of overflow
    obstacle_spawnNew();
    //spawn new obstable
  } else {
    //alternately, update everything
    //(I don't think we need to update
    //every frame do we?)
    for (ci = 0; ci < OBSTACLE_COUNT; ci++) {
      if (obstacle_type[ci] != OBSTACLE_TYPE_UNUSED) {
        obstacle_updateOne();
      }
    }
  }

  camera_lastMsbY = MSB(camera_y);
}

void obstacle_renderAll(void) {
  if (OB_LASERS_ACTIVE) {
  //not sure if we need to do anything here yet
    for (tempJ = 0; tempJ < OBSTACLE_COUNT; ++tempJ) {
      if (obstacle_type[tempJ] == OBSTACLE_TYPE_LASERS) {

        //save screen into tempD
        //(since I'm killing ci)
        tempD = obstacle_screen[tempJ];

        //calculate the X position
        ci = timer & B00000111;
        if (ci == 4) {
          ci = 8;
        }
        ci = (ci * 2) * 8 + 48;

        //calc the y
        cj = obstacle_y[tempJ];
        cj = cj * 8 + 8;
        temp = (240 - MSB(camera_y));
        if(tempD != camera_screen) {
          temp -= 16;
        }
        cj += temp;


        temp = 0xD6;
        if (timer & 1) {
          temp += 2;
        }
        temp2 = 3;
        tempA = 4;
        tempB = 2;

        sprite_drawBlock();
        return;

      }

    }
  }
}
