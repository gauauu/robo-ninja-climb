#define __NES__ 1

//make other IDEs, etc, happy
#ifndef __CC65__
#define __fastcall__
#define __asm //
#endif

#include "nes.h"
#include "binary.h"

#define u8 unsigned char
#define s8 signed char

#define u16 unsigned int
#define s16 signed int

#define bool u8
#define true 1
#define false 0


#define PAD_A       128
#define PAD_B       64
#define PAD_SELECT  32
#define PAD_START   16
#define PAD_UP      8
#define PAD_DOWN    4
#define PAD_LEFT    2
#define PAD_RIGHT   1

#define FACING_RIGHT 0
#define FACING_LEFT  1

#define FLIP_HORIZ B01000000
