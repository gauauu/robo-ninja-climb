.include "common.inc"

.import _startUp
.import title_nt
.import title_pal
.import sprite_palette
.import _ppu_screenOff
.import _sprite_oamIndex
.import FamiToneInit
.import FamiToneMusicPlay
.import song2_music_data
.import _drawTextCentered
.import _drawText
.importzp _tempPtr
.importzp _tempPtr2
.import _text_nt0



.export _dead_frames
_dead_frames:
  .byt $00
  .byt $00
  .byt $00
  .byt $01
  .byt $01
  .byt $01
  .byt $00
  .byt $00
  .byt $01
  .byt $01
  .byt $00
  .byt $01
  .byt $02
  .byt $02
  .byt $02
  .byt $02
  .byt $02
  .byt $02

.export _dead_gameOverScreenSetup
.proc _dead_gameOverScreenSetup

 jsr _ppu_screenOff

  ;load title palette
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR
copypalloop:
  lda title_pal,x
  sta PPUDATA
  inx
  cpx #16
  bcc copypalloop


  ;load roboninja-palette
  ;but don't bg color
  lda #$3f
  sta PPUDATA
  lda sprite_palette+1
  sta PPUDATA
  lda sprite_palette+2
  sta PPUDATA
  lda sprite_palette+3
  sta PPUDATA



  ;select 2nd chr bank
dead_bankSwitch:
  lda #01
  sta dead_bankSwitch+1


  ;load title screen and nametables
  lda #$20
  sta PPUADDR
  lda #$00
  sta PPUADDR

  ldy #30
  lda #$FF
:
  ldx #32
:
    sta PPUDATA
    dex
    bne :-
  dey
  bne :--

  lda #$21
  sta PPUADDR
  lda #$40
  sta PPUADDR

  ldy #8
  lda #$00
:
  ldx #32
:
    sta PPUDATA
    dex
    bne :-
  dey
  bne :--

  ;clear title attributes
  lda #$23
  sta PPUADDR
  lda #$C0
  sta PPUADDR
  ldx #64
  lda #0
:
  sta PPUDATA
  dex
  bne :-
  
  rts
.endproc
