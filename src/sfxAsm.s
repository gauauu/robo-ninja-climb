.include "global.inc"

.import FamiToneSfxInit
.import FamiToneSfxPlay

.import sounds

.export _sfx_init
.proc _sfx_init
  
  lda #1;NTSC
  ldx #<sounds
  ldy #>sounds
  jmp FamiToneSfxInit

.endproc

.export _sfx_playSound
.proc _sfx_playSound

  ldx #0
  jmp FamiToneSfxPlay

.endproc
