#include "common.h"

#define SFX_HIT       0
#define SFX_POWERUP   1

void sfx_init(void);
void __fastcall__ sfx_playSound(int sfx);
