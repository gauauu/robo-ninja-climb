#define waitVBlank()  while (!VBLANK_FLAG){}; VBLANK_FLAG = 0;

void ppu_screenOff(void);

/**
 * assumes a is nametable to show
 * x is hscroll
 * y is vscroll
 */
void ppu_screenOn(void);

/**
 * calls ppu_screenOn with top-left defaults
 */
void ppu_screenOnMain(void);

void __fastcall__ ppu_loadPalette(int levelPalette);

void ppu_showLeftNametableAtYScroll(void);

void ppu_profileStart(void);
void ppu_profileEnd(void);


