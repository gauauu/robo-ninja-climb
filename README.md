# README #

Robo-Ninja-Climb is my entry into the 2017 NesDev competition

# LICENSE #

License coming soon. Feel free to view the code for now, and ask me if you want to do something with it.

# ATTRIBUTION #

Background graphics: Omega Team by surt
https://opengameart.org/content/omega-team
https://creativecommons.org/licenses/by/3.0/
Modified to fit game palette, nes layout, etc

Computer Font: By WhoaMan
http://forums.nesdev.com/viewtopic.php?f=2&t=10284

Robo-Ninja character by Chris Hildenbrand

# TODO #

* improve music
* fly away at ending
